#include "runtime_ibl_c_api.h"
#include <GL/glew.h>
#include <GL/GL.h>
int main(int argc, char* argv[])
{
    // Create Context First
    glewExperimental = GL_TRUE;
    glewInit();
    auto cm = ibl_hdr_load("C:/Users/alexqzhou/Documents/WXWork/1688851418267126/Cache/File/2018-08/zhouqin_sky.hdr");
    auto dcm = ibl_create_cubemap(128, 128, 7);
    ibl_cubemap_filter_and_generate_mips(cm, dcm, 80, true, ibl_cubemap_get_max_illum(cm));
    ibl_cubemap_save_dds(dcm, "final_rgbm.dds");
    return 0;
}