//--------------------------------------------------------------------------------------
//CCubeMapFilter
// Classes for filtering and processing cubemaps
//
//
//--------------------------------------------------------------------------------------
// (C) 2005 ATI Research, Inc., All rights reserved.
//--------------------------------------------------------------------------------------
#include "CCubeMapProcessor.h"

#define CP_PI   3.14159265358979323846

//Filtering options for each thread
SThreadOptionsThread0 sg_FilterOptionsCPU0;

//Thread functions used to run filtering routines in the CPU0 process
// note that these functions can not be member functions since thread initiation
// must start from a global or static global function
DWORD WINAPI ThreadProcCPU0Filter(LPVOID a_NothingToPassToThisFunction)
{   
   //filter cube map mipchain
   sg_FilterOptionsCPU0.m_cmProc->FilterCubeMapMipChain(
	  sg_FilterOptionsCPU0.m_BaseFilterAngle,
	  sg_FilterOptionsCPU0.m_InitialMipAngle,
	  sg_FilterOptionsCPU0.m_MipAnglePerLevelScale,
	  sg_FilterOptionsCPU0.m_FilterType,
	  // SL BEGIN
	  // sg_FilterOptionsCPU0.m_FixupType,
	  // SL END
	  sg_FilterOptionsCPU0.m_FixupWidth,
	  sg_FilterOptionsCPU0.m_bUseSolidAngle 
	  // SL BEGIN
	  ,sg_FilterOptionsCPU0.m_MCO
	  // SL END
  );

   return(CP_THREAD_COMPLETED);
}

// SL BEGIN
DWORD WINAPI ThreadProcCPU0FilterMultithread(LPVOID a_NothingToPassToThisFunction)
{   

   //filter cube map mipchain
  sg_FilterOptionsCPU0.m_cmProc->FilterCubeMapMipChainMultithread(
	  sg_FilterOptionsCPU0.m_BaseFilterAngle,
	  sg_FilterOptionsCPU0.m_InitialMipAngle,
	  sg_FilterOptionsCPU0.m_MipAnglePerLevelScale,
	  sg_FilterOptionsCPU0.m_FilterType,
	  sg_FilterOptionsCPU0.m_FixupWidth,
	  sg_FilterOptionsCPU0.m_bUseSolidAngle,
	  sg_FilterOptionsCPU0.m_MCO
	  );

   return(CP_THREAD_COMPLETED);
}
// SL END

//Filtering options for each thread
SThreadOptionsThread1 sg_FilterOptionsCPU1;

//Thread functions used to run filtering routines in the process for CPU1
// note that these functions can not be member functions since thread initiation
// must start from a global or static global function

//This thread entry point function is called by thread 1  which is initiated
// by thread 0
DWORD WINAPI ThreadProcCPU1Filter(LPVOID a_NothingToPassToThisFunction)
{   
   //filter subset of faces 
   sg_FilterOptionsCPU1.m_cmProc->FilterCubeSurfaces(
      sg_FilterOptionsCPU1.m_SrcCubeMap, 
      sg_FilterOptionsCPU1.m_DstCubeMap, 
      sg_FilterOptionsCPU1.m_FilterConeAngle, 
      sg_FilterOptionsCPU1.m_FilterType, 
      sg_FilterOptionsCPU1.m_bUseSolidAngle, 
      sg_FilterOptionsCPU1.m_FaceIdxStart,
      sg_FilterOptionsCPU1.m_FaceIdxEnd,
      sg_FilterOptionsCPU1.m_ThreadIdx 
	  // SL BEGIN
	  ,sg_FilterOptionsCPU1.m_MCO.SpecularPower
	  ,sg_FilterOptionsCPU1.m_MCO.LightingModel
	  ,sg_FilterOptionsCPU1.m_MCO.FixupType
	  // SL END
	  ); 
      
   return(CP_THREAD_COMPLETED);
}


//------------------------------------------------------------------------------
// D3D cube map face specification
//   mapping from 3D x,y,z cube map lookup coordinates 
//   to 2D within face u,v coordinates
//
//   --------------------> U direction 
//   |                   (within-face texture space)
//   |         _____
//   |        |     |
//   |        | +Y  |
//   |   _____|_____|_____ _____
//   |  |     |     |     |     |
//   |  | -X  | +Z  | +X  | -Z  |
//   |  |_____|_____|_____|_____|
//   |        |     |
//   |        | -Y  |
//   |        |_____|
//   |
//   v   V direction
//      (within-face texture space)
//------------------------------------------------------------------------------

//Information about neighbors and how texture coorrdinates change across faces 
//  in ORDER of left, right, top, bottom (e.g. edges corresponding to u=0, 
//  u=1, v=0, v=1 in the 2D coordinate system of the particular face.
//Note this currently assumes the D3D cube face ordering and orientation
CPCubeMapNeighbor sg_CubeNgh[6][4] =
{
    //XPOS face
    {{CP_FACE_Z_POS, CP_EDGE_RIGHT },
     {CP_FACE_Z_NEG, CP_EDGE_LEFT  },
     {CP_FACE_Y_POS, CP_EDGE_RIGHT },
     {CP_FACE_Y_NEG, CP_EDGE_RIGHT }},
    //XNEG face
    {{CP_FACE_Z_NEG, CP_EDGE_RIGHT },
     {CP_FACE_Z_POS, CP_EDGE_LEFT  },
     {CP_FACE_Y_POS, CP_EDGE_LEFT  },
     {CP_FACE_Y_NEG, CP_EDGE_LEFT  }},
    //YPOS face
    {{CP_FACE_X_NEG, CP_EDGE_TOP },
     {CP_FACE_X_POS, CP_EDGE_TOP },
     {CP_FACE_Z_NEG, CP_EDGE_TOP },
     {CP_FACE_Z_POS, CP_EDGE_TOP }},
    //YNEG face
    {{CP_FACE_X_NEG, CP_EDGE_BOTTOM},
     {CP_FACE_X_POS, CP_EDGE_BOTTOM},
     {CP_FACE_Z_POS, CP_EDGE_BOTTOM},
     {CP_FACE_Z_NEG, CP_EDGE_BOTTOM}},
    //ZPOS face
    {{CP_FACE_X_NEG, CP_EDGE_RIGHT  },
     {CP_FACE_X_POS, CP_EDGE_LEFT   },
     {CP_FACE_Y_POS, CP_EDGE_BOTTOM },
     {CP_FACE_Y_NEG, CP_EDGE_TOP    }},
    //ZNEG face
    {{CP_FACE_X_POS, CP_EDGE_RIGHT  },
     {CP_FACE_X_NEG, CP_EDGE_LEFT   },
     {CP_FACE_Y_POS, CP_EDGE_TOP    },
     {CP_FACE_Y_NEG, CP_EDGE_BOTTOM }}
};


//3x2 matrices that map cube map indexing vectors in 3d 
// (after face selection and divide through by the 
//  _ABSOLUTE VALUE_ of the max coord)
// into NVC space
//Note this currently assumes the D3D cube face ordering and orientation
#define CP_UDIR     0
#define CP_VDIR     1
#define CP_FACEAXIS 2

float32 sgFace2DMapping[6][3][3] = {
    //XPOS face
    {{ 0,  0, -1},   //u towards negative Z
     { 0, -1,  0},   //v towards negative Y
     {1,  0,  0}},  //pos X axis  
    //XNEG face
     {{0,  0,  1},   //u towards positive Z
      {0, -1,  0},   //v towards negative Y
      {-1,  0,  0}},  //neg X axis       
    //YPOS face
    {{1, 0, 0},     //u towards positive X
     {0, 0, 1},     //v towards positive Z
     {0, 1 , 0}},   //pos Y axis  
    //YNEG face
    {{1, 0, 0},     //u towards positive X
     {0, 0 , -1},   //v towards negative Z
     {0, -1 , 0}},  //neg Y axis  
    //ZPOS face
    {{1, 0, 0},     //u towards positive X
     {0, -1, 0},    //v towards negative Y
     {0, 0,  1}},   //pos Z axis  
    //ZNEG face
    {{-1, 0, 0},    //u towards negative X
     {0, -1, 0},    //v towards negative Y
     {0, 0, -1}},   //neg Z axis  
};


//The 12 edges of the cubemap, (entries are used to index into the neighbor table)
// this table is used to average over the edges.
int32 sg_CubeEdgeList[12][2] = {
   {CP_FACE_X_POS, CP_EDGE_LEFT},
   {CP_FACE_X_POS, CP_EDGE_RIGHT},
   {CP_FACE_X_POS, CP_EDGE_TOP},
   {CP_FACE_X_POS, CP_EDGE_BOTTOM},

   {CP_FACE_X_NEG, CP_EDGE_LEFT},
   {CP_FACE_X_NEG, CP_EDGE_RIGHT},
   {CP_FACE_X_NEG, CP_EDGE_TOP},
   {CP_FACE_X_NEG, CP_EDGE_BOTTOM},

   {CP_FACE_Z_POS, CP_EDGE_TOP},
   {CP_FACE_Z_POS, CP_EDGE_BOTTOM},
   {CP_FACE_Z_NEG, CP_EDGE_TOP},
   {CP_FACE_Z_NEG, CP_EDGE_BOTTOM}
};


//Information about which of the 8 cube corners are correspond to the 
//  the 4 corners in each cube face
//  the order is upper left, upper right, lower left, lower right
int32 sg_CubeCornerList[6][4] = {
   { CP_CORNER_PPP, CP_CORNER_PPN, CP_CORNER_PNP, CP_CORNER_PNN }, // XPOS face
   { CP_CORNER_NPN, CP_CORNER_NPP, CP_CORNER_NNN, CP_CORNER_NNP }, // XNEG face
   { CP_CORNER_NPN, CP_CORNER_PPN, CP_CORNER_NPP, CP_CORNER_PPP }, // YPOS face
   { CP_CORNER_NNP, CP_CORNER_PNP, CP_CORNER_NNN, CP_CORNER_PNN }, // YNEG face
   { CP_CORNER_NPP, CP_CORNER_PPP, CP_CORNER_NNP, CP_CORNER_PNP }, // ZPOS face
   { CP_CORNER_PPN, CP_CORNER_NPN, CP_CORNER_PNN, CP_CORNER_NNN }  // ZNEG face
};


//--------------------------------------------------------------------------------------
//Error handling for cube map processor
//  Pop up dialog box, and terminate application
//--------------------------------------------------------------------------------------
void CPFatalError(WCHAR *a_Msg)
{
   MessageBoxW(NULL, a_Msg, L"Error: Application Terminating", MB_OK);
   exit(EM_FATAL_ERROR);
}

// SL BEGIN
void slerp(float32* res, float32* a, float32* b, float32 t)
{
	float32 angle = acosf(VM_DOTPROD3(a, b));

	if (0.0f == angle)
	{
		res[0] = a[0];
		res[1] = a[1];
		res[2] = a[2];
	}
	else if (CP_PI == angle)
	{
		// Can't recovert!
		res[0] = 0;
		res[1] = 0;
		res[2] = 0;
	}
	else
	{
		res[0] = (sinf((1.0-t)*angle)/sinf(angle))*a[0] + (sinf(t*angle)/sinf(angle))*b[0];
		res[1] = (sinf((1.0-t)*angle)/sinf(angle))*a[1] + (sinf(t*angle)/sinf(angle))*b[1];
		res[2] = (sinf((1.0-t)*angle)/sinf(angle))*a[2] + (sinf(t*angle)/sinf(angle))*b[2];
	}
}

#define LERP(A, B, FACTOR) ( (A) + (FACTOR)*((B) - (A)) )	
// SL END

//--------------------------------------------------------------------------------------
// Convert cubemap face texel coordinates and face idx to 3D vector
// note the U and V coords are integer coords and range from 0 to size-1
//  this routine can be used to generate a normalizer cube map
//--------------------------------------------------------------------------------------
// SL BEGIN
void TexelCoordToVect(int32 a_FaceIdx, float32 a_U, float32 a_V, int32 a_Size, float32 *a_XYZ, int32 a_FixupType)
{
	float32 nvcU, nvcV;
	float32 tempVec[3];

	if (a_FixupType == CP_FIXUP_STRETCH && a_Size > 1)
	{
		// Code from Nvtt : http://code.google.com/p/nvidia-texture-tools/source/browse/trunk/src/nvtt/CubeSurface.cpp		
		// transform from [0..res - 1] to [-1 .. 1], match up edges exactly.
		nvcU = (2.0f * (float32)a_U / ((float32)a_Size - 1.0f) ) - 1.0f;
		nvcV = (2.0f * (float32)a_V / ((float32)a_Size - 1.0f) ) - 1.0f;
	}
	else
	{
		// Change from original AMD code
		// transform from [0..res - 1] to [- (1 - 1 / res) .. (1 - 1 / res)]
		// + 0.5f is for texel center addressing
		nvcU = (2.0f * ((float32)a_U + 0.5f) / (float32)a_Size ) - 1.0f;
		nvcV = (2.0f * ((float32)a_V + 0.5f) / (float32)a_Size ) - 1.0f;
	}

	if (a_FixupType == CP_FIXUP_WARP && a_Size > 1)
	{
		// Code from Nvtt : http://code.google.com/p/nvidia-texture-tools/source/browse/trunk/src/nvtt/CubeSurface.cpp
		float32 a = powf(float32(a_Size), 2.0f) / powf(float32(a_Size - 1), 3.0f);
		nvcU = a * powf(nvcU, 3) + nvcU;
		nvcV = a * powf(nvcV, 3) + nvcV;

		// Get current vector
		//generate x,y,z vector (xform 2d NVC coord to 3D vector)
		//U contribution
		VM_SCALE3(a_XYZ, sgFace2DMapping[a_FaceIdx][CP_UDIR], nvcU);    
		//V contribution
		VM_SCALE3(tempVec, sgFace2DMapping[a_FaceIdx][CP_VDIR], nvcV);
		VM_ADD3(a_XYZ, tempVec, a_XYZ);
		//add face axis
		VM_ADD3(a_XYZ, sgFace2DMapping[a_FaceIdx][CP_FACEAXIS], a_XYZ);
		//normalize vector
		VM_NORM3(a_XYZ, a_XYZ);
	}
	else if (a_FixupType == CP_FIXUP_BENT && a_Size > 1)
	{
		// Method following description of Physically based rendering slides from CEDEC2011 of TriAce

		// Get vector at edge
		float32 EdgeNormalU[3];
		float32 EdgeNormalV[3];
		float32 EdgeNormal[3];
		float32 EdgeNormalMinusOne[3];

		// Recover vector at edge
		//U contribution
		VM_SCALE3(EdgeNormalU, sgFace2DMapping[a_FaceIdx][CP_UDIR], nvcU < 0.0 ? -1.0f : 1.0f);    
		//V contribution
		VM_SCALE3(EdgeNormalV, sgFace2DMapping[a_FaceIdx][CP_VDIR], nvcV < 0.0 ? -1.0f : 1.0f);
		VM_ADD3(EdgeNormal, EdgeNormalV, EdgeNormalU);
		//add face axis
		VM_ADD3(EdgeNormal, sgFace2DMapping[a_FaceIdx][CP_FACEAXIS], EdgeNormal);
		//normalize vector
		VM_NORM3(EdgeNormal, EdgeNormal);

		// Get vector at (edge - 1)
		float32 nvcUEdgeMinus1 = (2.0f * ((float32)(nvcU < 0.0f ? 0 : a_Size-1) + 0.5f) / (float32)a_Size ) - 1.0f;
		float32 nvcVEdgeMinus1 = (2.0f * ((float32)(nvcV < 0.0f ? 0 : a_Size-1) + 0.5f) / (float32)a_Size ) - 1.0f;

		// Recover vector at (edge - 1)
		//U contribution
		VM_SCALE3(EdgeNormalU, sgFace2DMapping[a_FaceIdx][CP_UDIR], nvcUEdgeMinus1);    
		//V contribution
		VM_SCALE3(EdgeNormalV, sgFace2DMapping[a_FaceIdx][CP_VDIR], nvcVEdgeMinus1);
		VM_ADD3(EdgeNormalMinusOne, EdgeNormalV, EdgeNormalU);
		//add face axis
		VM_ADD3(EdgeNormalMinusOne, sgFace2DMapping[a_FaceIdx][CP_FACEAXIS], EdgeNormalMinusOne);
		//normalize vector
		VM_NORM3(EdgeNormalMinusOne, EdgeNormalMinusOne);

		// Get angle between the two vector (which is 50% of the two vector presented in the TriAce slide)
		float32 AngleNormalEdge = acosf(VM_DOTPROD3(EdgeNormal, EdgeNormalMinusOne));
		
		// Here we assume that high resolution required less offset than small resolution (TriAce based this on blur radius and custom value) 
		// Start to increase from 50% to 100% target angle from 128x128x6 to 1x1x6
		float32 NumLevel = (logf(min(a_Size, 128))  / logf(2)) - 1;
		AngleNormalEdge = LERP(0.5 * AngleNormalEdge, AngleNormalEdge, 1.0f - (NumLevel/6) );

		float32 factorU = fabs((2.0f * ((float32)a_U) / (float32)(a_Size - 1) ) - 1.0f);
		float32 factorV = fabs((2.0f * ((float32)a_V) / (float32)(a_Size - 1) ) - 1.0f);
		AngleNormalEdge = LERP(0.0f, AngleNormalEdge, max(factorU, factorV) );

		// Get current vector
		//generate x,y,z vector (xform 2d NVC coord to 3D vector)
		//U contribution
		VM_SCALE3(a_XYZ, sgFace2DMapping[a_FaceIdx][CP_UDIR], nvcU);    
		//V contribution
		VM_SCALE3(tempVec, sgFace2DMapping[a_FaceIdx][CP_VDIR], nvcV);
		VM_ADD3(a_XYZ, tempVec, a_XYZ);
		//add face axis
		VM_ADD3(a_XYZ, sgFace2DMapping[a_FaceIdx][CP_FACEAXIS], a_XYZ);
		//normalize vector
		VM_NORM3(a_XYZ, a_XYZ);

		float32 RadiantAngle = AngleNormalEdge;
		// Get angle between face normal and current normal. Used to push the normal away from face normal.
		float32 AngleFaceVector = acosf(VM_DOTPROD3(sgFace2DMapping[a_FaceIdx][CP_FACEAXIS], a_XYZ));

		// Push the normal away from face normal by an angle of RadiantAngle
		slerp(a_XYZ, sgFace2DMapping[a_FaceIdx][CP_FACEAXIS], a_XYZ, 1.0f + RadiantAngle / AngleFaceVector);
	}
	else
	{
		//generate x,y,z vector (xform 2d NVC coord to 3D vector)
		//U contribution
		VM_SCALE3(a_XYZ, sgFace2DMapping[a_FaceIdx][CP_UDIR], nvcU);    
		//V contribution
		VM_SCALE3(tempVec, sgFace2DMapping[a_FaceIdx][CP_VDIR], nvcV);
		VM_ADD3(a_XYZ, tempVec, a_XYZ);
		//add face axis
		VM_ADD3(a_XYZ, sgFace2DMapping[a_FaceIdx][CP_FACEAXIS], a_XYZ);

		//normalize vector
		VM_NORM3(a_XYZ, a_XYZ);
	}
}
// SL END

//--------------------------------------------------------------------------------------
// Convert 3D vector to cubemap face texel coordinates and face idx 
// note the U and V coords are integer coords and range from 0 to size-1
//  this routine can be used to generate a normalizer cube map
//
// returns face IDX and texel coords
//--------------------------------------------------------------------------------------
// SL BEGIN
/*
Mapping Texture Coordinates to Cube Map Faces
Because there are multiple faces, the mapping of texture coordinates to positions on cube map faces
is more complicated than the other texturing targets.  The EXT_texture_cube_map extension is purposefully
designed to be consistent with DirectX 7's cube map arrangement.  This is also consistent with the cube
map arrangement in Pixar's RenderMan package. 
For cube map texturing, the (s,t,r) texture coordinates are treated as a direction vector (rx,ry,rz)
emanating from the center of a cube.  (The q coordinate can be ignored since it merely scales the vector
without affecting the direction.) At texture application time, the interpolated per-fragment (s,t,r)
selects one of the cube map face's 2D mipmap sets based on the largest magnitude coordinate direction 
the major axis direction). The target column in the table below explains how the major axis direction
maps to the 2D image of a particular cube map target. 

major axis 
direction     target                              sc     tc    ma 
----------    ---------------------------------   ---    ---   --- 
+rx          GL_TEXTURE_CUBE_MAP_POSITIVE_X_EXT   -rz    -ry   rx 
-rx          GL_TEXTURE_CUBE_MAP_NEGATIVE_X_EXT   +rz    -ry   rx 
+ry          GL_TEXTURE_CUBE_MAP_POSITIVE_Y_EXT   +rx    +rz   ry 
-ry          GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_EXT   +rx    -rz   ry 
+rz          GL_TEXTURE_CUBE_MAP_POSITIVE_Z_EXT   +rx    -ry   rz 
-rz          GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_EXT   -rx    -ry   rz

Using the sc, tc, and ma determined by the major axis direction as specified in the table above,
an updated (s,t) is calculated as follows 
s   =   ( sc/|ma| + 1 ) / 2 
t   =   ( tc/|ma| + 1 ) / 2
If |ma| is zero or very nearly zero, the results of the above two equations need not be defined
(though the result may not lead to GL interruption or termination).  Once the cube map face's 2D mipmap
set and (s,t) is determined, texture fetching and filtering proceeds like standard OpenGL 2D texturing. 
*/
// Note this method return U and V in range from 0 to size-1
// SL END
void VectToTexelCoord(float32 *a_XYZ, int32 a_Size, int32 *a_FaceIdx, int32 *a_U, int32 *a_V )
{
   float32 nvcU, nvcV;
   float32 absXYZ[3];
   float32 maxCoord;
   float32 onFaceXYZ[3];
   int32   faceIdx;
   int32   u, v;

   //absolute value 3
   VM_ABS3(absXYZ, a_XYZ);

   if( (absXYZ[0] >= absXYZ[1]) && (absXYZ[0] >= absXYZ[2]) )
   {
      maxCoord = absXYZ[0];

      if(a_XYZ[0] >= 0) //face = XPOS
      {
         faceIdx = CP_FACE_X_POS;            
      }    
      else
      {
         faceIdx = CP_FACE_X_NEG;                    
      }
   }
   else if ( (absXYZ[1] >= absXYZ[0]) && (absXYZ[1] >= absXYZ[2]) )
   {
      maxCoord = absXYZ[1];

      if(a_XYZ[1] >= 0) //face = XPOS
      {
         faceIdx = CP_FACE_Y_POS;            
      }    
      else
      {
         faceIdx = CP_FACE_Y_NEG;                    
      }    
   }
   else  // if( (absXYZ[2] > absXYZ[0]) && (absXYZ[2] > absXYZ[1]) )
   {
      maxCoord = absXYZ[2];

      if(a_XYZ[2] >= 0) //face = XPOS
      {
         faceIdx = CP_FACE_Z_POS;            
      }    
      else
      {
         faceIdx = CP_FACE_Z_NEG;                    
      }    
   }

   //divide through by max coord so face vector lies on cube face
   VM_SCALE3(onFaceXYZ, a_XYZ, 1.0f/maxCoord);
   nvcU = VM_DOTPROD3(sgFace2DMapping[ faceIdx ][CP_UDIR], onFaceXYZ );
   nvcV = VM_DOTPROD3(sgFace2DMapping[ faceIdx ][CP_VDIR], onFaceXYZ );

   // SL BEGIN
   // Modify original AMD code to return value from 0 to Size - 1
   u = (int32)floor( (a_Size - 1) * 0.5f * (nvcU + 1.0f) );
   v = (int32)floor( (a_Size - 1) * 0.5f * (nvcV + 1.0f) );
   // SL END

   *a_FaceIdx = faceIdx;
   *a_U = u;
   *a_V = v;
}


//--------------------------------------------------------------------------------------
// gets texel ptr in a cube map given a direction vector, and an array of 
//  CImageSurfaces that represent the cube faces.
//   
//--------------------------------------------------------------------------------------
CP_ITYPE *GetCubeMapTexelPtr(float32 *a_XYZ, CImageSurface *a_Surface)
{
   int32 u, v, faceIdx;    

   //get face idx and u, v texel coordinate in face
   VectToTexelCoord(a_XYZ, a_Surface[0].m_Width, &faceIdx, &u, &v );

   return( a_Surface[faceIdx].GetSurfaceTexelPtr(u, v) );
}


//--------------------------------------------------------------------------------------
//  Compute solid angle of given texel in cubemap face for weighting taps in the 
//   kernel by the area they project to on the unit sphere.
//
//  Note that this code uses an approximation to the solid angle, by treating the 
//   two triangles that make up the quad comprising the texel as planar.  If more
//   accuracy is required, the solid angle per triangle lying on the sphere can be 
//   computed using the sum of the interior angles - PI.
//
//--------------------------------------------------------------------------------------
// SL BEGIN
// Replace the calcul of the solidAngle by a better approximation.
#if 0
float32 TexelCoordSolidAngle(int32 a_FaceIdx, float32 a_U, float32 a_V, int32 a_Size)
{
   float32 cornerVect[4][3];
   float64 cornerVect64[4][3];

   float32 halfTexelStep = 0.5f;  //note u, and v are in texel coords (where each texel is one unit)  
   float64 edgeVect0[3];
   float64 edgeVect1[3];
   float64 xProdVect[3];
   float64 texelArea;

   //compute 4 corner vectors of texel
   TexelCoordToVect(a_FaceIdx, a_U - halfTexelStep, a_V - halfTexelStep, a_Size, cornerVect[0] );
   TexelCoordToVect(a_FaceIdx, a_U - halfTexelStep, a_V + halfTexelStep, a_Size, cornerVect[1] );
   TexelCoordToVect(a_FaceIdx, a_U + halfTexelStep, a_V - halfTexelStep, a_Size, cornerVect[2] );
   TexelCoordToVect(a_FaceIdx, a_U + halfTexelStep, a_V + halfTexelStep, a_Size, cornerVect[3] );
   
   VM_NORM3_UNTYPED(cornerVect64[0], cornerVect[0] );
   VM_NORM3_UNTYPED(cornerVect64[1], cornerVect[1] );
   VM_NORM3_UNTYPED(cornerVect64[2], cornerVect[2] );
   VM_NORM3_UNTYPED(cornerVect64[3], cornerVect[3] );

   //area of triangle defined by corners 0, 1, and 2
   VM_SUB3_UNTYPED(edgeVect0, cornerVect64[1], cornerVect64[0] );
   VM_SUB3_UNTYPED(edgeVect1, cornerVect64[2], cornerVect64[0] );    
   VM_XPROD3_UNTYPED(xProdVect, edgeVect0, edgeVect1 );
   texelArea = 0.5f * sqrt( VM_DOTPROD3_UNTYPED(xProdVect, xProdVect ) );

   //area of triangle defined by corners 1, 2, and 3
   VM_SUB3_UNTYPED(edgeVect0, cornerVect64[2], cornerVect64[1] );
   VM_SUB3_UNTYPED(edgeVect1, cornerVect64[3], cornerVect64[1] );
   VM_XPROD3_UNTYPED(xProdVect, edgeVect0, edgeVect1 );
   texelArea += 0.5f * sqrt( VM_DOTPROD3_UNTYPED(xProdVect, xProdVect ) );

   return texelArea;
}
#else

/** Original code from Ignacio Casta駉
* This formula is from Manne 謍rstr鰉's thesis.
* Take two coordiantes in the range [-1, 1] that define a portion of a
* cube face and return the area of the projection of that portion on the
* surface of the sphere.
**/

static float32 AreaElement( float32 x, float32 y )
{
	return atan2(x * y, sqrt(x * x + y * y + 1));
}

float32 TexelCoordSolidAngle(int32 a_FaceIdx, float32 a_U, float32 a_V, int32 a_Size)
{
   // transform from [0..res - 1] to [- (1 - 1 / res) .. (1 - 1 / res)]
   // (+ 0.5f is for texel center addressing)
   float32 U = (2.0f * ((float32)a_U + 0.5f) / (float32)a_Size ) - 1.0f;
   float32 V = (2.0f * ((float32)a_V + 0.5f) / (float32)a_Size ) - 1.0f;

   // Shift from a demi texel, mean 1.0f / a_Size with U and V in [-1..1]
   float32 InvResolution = 1.0f / a_Size;

	// U and V are the -1..1 texture coordinate on the current face.
	// Get projected area for this texel
	float32 x0 = U - InvResolution;
	float32 y0 = V - InvResolution;
	float32 x1 = U + InvResolution;
	float32 y1 = V + InvResolution;
	float32 SolidAngle = AreaElement(x0, y0) - AreaElement(x0, y1) - AreaElement(x1, y0) + AreaElement(x1, y1);

	return SolidAngle;
}
#endif
// SL END


//--------------------------------------------------------------------------------------
//Builds a normalizer cubemap
//
// Takes in a cube face size, and an array of 6 surfaces to write the cube faces into
//
// Note that this normalizer cube map stores the vectors in unbiased -1 to 1 range.
//  if _bx2 style scaled and biased vectors are needed, uncomment the SCALE and BIAS
//  below
//--------------------------------------------------------------------------------------
// SL BEGIN
void CCubeMapProcessor::BuildNormalizerCubemap(int32 a_Size, CImageSurface *a_Surface, int32 a_FixupType)
// SL END
{
   int32 iCubeFace, u, v;

   //iterate over cube faces
   for(iCubeFace=0; iCubeFace<6; iCubeFace++)
   {
      a_Surface[iCubeFace].Clear();
      a_Surface[iCubeFace].Init(a_Size, a_Size, 3);

      //fast texture walk, build normalizer cube map
      CP_ITYPE *texelPtr = a_Surface[iCubeFace].m_ImgData;

      for(v=0; v < a_Surface[iCubeFace].m_Height; v++)
      {
         for(u=0; u < a_Surface[iCubeFace].m_Width; u++)
         {
			 // SL_BEGIN
            TexelCoordToVect(iCubeFace, (float32)u, (float32)v, a_Size, texelPtr, a_FixupType);
			// SL END

            //VM_SCALE3(texelPtr, texelPtr, 0.5f);
            //VM_BIAS3(texelPtr, texelPtr, 0.5f);

            texelPtr += a_Surface[iCubeFace].m_NumChannels;
         }         
      }
   }
}


//--------------------------------------------------------------------------------------
//Builds a normalizer cubemap, with the texels solid angle stored in the fourth component
//
//Takes in a cube face size, and an array of 6 surfaces to write the cube faces into
//
//Note that this normalizer cube map stores the vectors in unbiased -1 to 1 range.
// if _bx2 style scaled and biased vectors are needed, uncomment the SCALE and BIAS
// below
//--------------------------------------------------------------------------------------
// SL BEGIN
void CCubeMapProcessor::BuildNormalizerSolidAngleCubemap(int32 a_Size, CImageSurface *a_Surface, int32 a_FixupType)
// SL END
{
   int32 iCubeFace, u, v;

   //iterate over cube faces
   for(iCubeFace=0; iCubeFace<6; iCubeFace++)
   {
      a_Surface[iCubeFace].Clear();
      a_Surface[iCubeFace].Init(a_Size, a_Size, 4);  //First three channels for norm cube, and last channel for solid angle

      //fast texture walk, build normalizer cube map
      CP_ITYPE *texelPtr = a_Surface[iCubeFace].m_ImgData;

      for(v=0; v<a_Surface[iCubeFace].m_Height; v++)
      {
         for(u=0; u<a_Surface[iCubeFace].m_Width; u++)
         {
			// SL_BEGIN
            TexelCoordToVect(iCubeFace, (float32)u, (float32)v, a_Size, texelPtr, a_FixupType);
			// SL END
            //VM_SCALE3(texelPtr, texelPtr, 0.5f);
            //VM_BIAS3(texelPtr, texelPtr, 0.5f);

            *(texelPtr + 3) = TexelCoordSolidAngle(iCubeFace, (float32)u, (float32)v, a_Size);

            texelPtr += a_Surface[iCubeFace].m_NumChannels;
         }         
      }
   }
}


//--------------------------------------------------------------------------------------
//Clear filter extents for the 6 cube map faces
//--------------------------------------------------------------------------------------
void CCubeMapProcessor::ClearFilterExtents(CBBoxInt32 *aFilterExtents)
{
   int32 iCubeFaces;

   for(iCubeFaces=0; iCubeFaces<6; iCubeFaces++)
   {
      aFilterExtents[iCubeFaces].Clear();    
   }
}


//--------------------------------------------------------------------------------------
//Define per-face bounding box filter extents
//
// These define conservative texel regions in each of the faces the filter can possibly 
// process.  When the pixels in the regions are actually processed, the dot product  
// between the tap vector and the center tap vector is used to determine the weight of 
// the tap and whether or not the tap is within the cone.
//
//--------------------------------------------------------------------------------------
void CCubeMapProcessor::DetermineFilterExtents(float32 *a_CenterTapDir, int32 a_SrcSize, int32 a_BBoxSize, 
                                               CBBoxInt32 *a_FilterExtents )
{
   int32 u, v;
   int32 faceIdx;
   int32 minU, minV, maxU, maxV;
   int32 i;

   //neighboring face and bleed over amount, and width of BBOX for
   // left, right, top, and bottom edges of this face
   int32 bleedOverAmount[4];
   int32 bleedOverBBoxMin[4];
   int32 bleedOverBBoxMax[4];

   int32 neighborFace;
   int32 neighborEdge;

   //get face idx, and u, v info from center tap dir
   VectToTexelCoord(a_CenterTapDir, a_SrcSize, &faceIdx, &u, &v );

   //define bbox size within face
   a_FilterExtents[faceIdx].Augment(u - a_BBoxSize, v - a_BBoxSize, 0);
   a_FilterExtents[faceIdx].Augment(u + a_BBoxSize, v + a_BBoxSize, 0);

   a_FilterExtents[faceIdx].ClampMin(0, 0, 0);
   a_FilterExtents[faceIdx].ClampMax(a_SrcSize-1, a_SrcSize-1, 0);

   //u and v extent in face corresponding to center tap
   minU = a_FilterExtents[faceIdx].m_minCoord[0];
   minV = a_FilterExtents[faceIdx].m_minCoord[1];
   maxU = a_FilterExtents[faceIdx].m_maxCoord[0];
   maxV = a_FilterExtents[faceIdx].m_maxCoord[1];

   //bleed over amounts for face across u=0 edge (left)    
   bleedOverAmount[0] = (a_BBoxSize - u);
   bleedOverBBoxMin[0] = minV;
   bleedOverBBoxMax[0] = maxV;

   //bleed over amounts for face across u=1 edge (right)    
   bleedOverAmount[1] = (u + a_BBoxSize) - (a_SrcSize-1);
   bleedOverBBoxMin[1] = minV;
   bleedOverBBoxMax[1] = maxV;

   //bleed over to face across v=0 edge (up)
   bleedOverAmount[2] = (a_BBoxSize - v);
   bleedOverBBoxMin[2] = minU;
   bleedOverBBoxMax[2] = maxU;

   //bleed over to face across v=1 edge (down)
   bleedOverAmount[3] = (v + a_BBoxSize) - (a_SrcSize-1);
   bleedOverBBoxMin[3] = minU;
   bleedOverBBoxMax[3] = maxU;

   //compute bleed over regions in neighboring faces
   for(i=0; i<4; i++)
   {
      if(bleedOverAmount[i] > 0)
      {
         neighborFace = sg_CubeNgh[faceIdx][i].m_Face;
         neighborEdge = sg_CubeNgh[faceIdx][i].m_Edge;

         //For certain types of edge abutments, the bleedOverBBoxMin, and bleedOverBBoxMax need to 
         //  be flipped: the cases are 
         // if a left   edge mates with a left or bottom  edge on the neighbor
         // if a top    edge mates with a top or right edge on the neighbor
         // if a right  edge mates with a right or top edge on the neighbor
         // if a bottom edge mates with a bottom or left  edge on the neighbor
         //Seeing as the edges are enumerated as follows 
         // left   =0 
         // right  =1 
         // top    =2 
         // bottom =3            
         // 
         // so if the edge enums are the same, or the sum of the enums == 3, 
         //  the bbox needs to be flipped
         if( (i == neighborEdge) || ((i+neighborEdge) == 3) )
         {
            bleedOverBBoxMin[i] = (a_SrcSize-1) - bleedOverBBoxMin[i];
            bleedOverBBoxMax[i] = (a_SrcSize-1) - bleedOverBBoxMax[i];
         }


         //The way the bounding box is extended onto the neighboring face
         // depends on which edge of neighboring face abuts with this one
         switch(sg_CubeNgh[faceIdx][i].m_Edge)
         {
            case CP_EDGE_LEFT:
               a_FilterExtents[neighborFace].Augment(0, bleedOverBBoxMin[i], 0);
               a_FilterExtents[neighborFace].Augment(bleedOverAmount[i], bleedOverBBoxMax[i], 0);
            break;
            case CP_EDGE_RIGHT:                
               a_FilterExtents[neighborFace].Augment( (a_SrcSize-1), bleedOverBBoxMin[i], 0);
               a_FilterExtents[neighborFace].Augment( (a_SrcSize-1) - bleedOverAmount[i], bleedOverBBoxMax[i], 0);
            break;
            case CP_EDGE_TOP:   
               a_FilterExtents[neighborFace].Augment(bleedOverBBoxMin[i], 0, 0);
               a_FilterExtents[neighborFace].Augment(bleedOverBBoxMax[i], bleedOverAmount[i], 0);
            break;
            case CP_EDGE_BOTTOM:   
               a_FilterExtents[neighborFace].Augment(bleedOverBBoxMin[i], (a_SrcSize-1), 0);
               a_FilterExtents[neighborFace].Augment(bleedOverBBoxMax[i], (a_SrcSize-1) - bleedOverAmount[i], 0);            
            break;
         }

         //clamp filter extents in non-center tap faces to remain within surface
         a_FilterExtents[neighborFace].ClampMin(0, 0, 0);
         a_FilterExtents[neighborFace].ClampMax(a_SrcSize-1, a_SrcSize-1, 0);
      }

      //If the bleed over amount bleeds past the adjacent face onto the opposite face 
      // from the center tap face, then process the opposite face entirely for now. 
      //Note that the cases in which this happens, what usually happens is that 
      // more than one edge bleeds onto the opposite face, and the bounding box 
      // encompasses the entire cube map face.
      if(bleedOverAmount[i] > a_SrcSize)
      {
         uint32 oppositeFaceIdx; 

         //determine opposite face 
         switch(faceIdx)
         {
            case CP_FACE_X_POS:
               oppositeFaceIdx = CP_FACE_X_NEG;
            break;
            case CP_FACE_X_NEG:
               oppositeFaceIdx = CP_FACE_X_POS;
            break;
            case CP_FACE_Y_POS:
               oppositeFaceIdx = CP_FACE_Y_NEG;
            break;
            case CP_FACE_Y_NEG:
               oppositeFaceIdx = CP_FACE_Y_POS;
            break;
            case CP_FACE_Z_POS:
               oppositeFaceIdx = CP_FACE_Z_NEG;
            break;
            case CP_FACE_Z_NEG:
               oppositeFaceIdx = CP_FACE_Z_POS;
            break;
            default:
            break;
         }
   
         //just encompass entire face for now
         a_FilterExtents[oppositeFaceIdx].Augment(0, 0, 0);
         a_FilterExtents[oppositeFaceIdx].Augment((a_SrcSize-1), (a_SrcSize-1), 0);            
      }
   }

   minV=minV;
}


//--------------------------------------------------------------------------------------
//ProcessFilterExtents 
//  Process bounding box in each cube face 
//
//--------------------------------------------------------------------------------------
void CCubeMapProcessor::ProcessFilterExtents(float32 *a_CenterTapDir, float32 a_DotProdThresh, 
    CBBoxInt32 *a_FilterExtents, CImageSurface *a_NormCubeMap, CImageSurface *a_SrcCubeMap, 
    CP_ITYPE *a_DstVal, uint32 a_FilterType, bool8 a_bUseSolidAngleWeighting
	// SL BEGIN
	,float32 a_SpecularPower
	,int32 a_LightingModel
	// SL END
	)
{
   int32 iFaceIdx, u, v;
   int32 faceWidth;
   int32 k;

   //pointers used to walk across the image surface to accumulate taps
   CP_ITYPE *normCubeRowStartPtr;
   CP_ITYPE *srcCubeRowStartPtr;
   CP_ITYPE *texelVect;


   //accumulators are 64-bit floats in order to have the precision needed 
   // over a summation of a large number of pixels 
   float64 dstAccum[4];
   float64 weightAccum;

   CP_ITYPE tapDotProd;   //dot product between center tap and current tap

   int32 normCubePitch;
   int32 srcCubePitch;
   int32 normCubeRowWalk;
   int32 srcCubeRowWalk;

   int32 uStart, uEnd;
   int32 vStart, vEnd;

   int32 nSrcChannels; 

   nSrcChannels = a_SrcCubeMap[0].m_NumChannels;

   //norm cube map and srcCubeMap have same face width
   faceWidth = a_NormCubeMap[0].m_Width;

   //amount to add to pointer to move to next scanline in images
   normCubePitch = faceWidth * a_NormCubeMap[0].m_NumChannels;
   srcCubePitch = faceWidth * a_SrcCubeMap[0].m_NumChannels;

   //dest accum
   for(k=0; k<m_NumChannels; k++)
   {
      dstAccum[k] = 0.0f;
   }

   weightAccum = 0.0f;

   // SL BEGIN
   // Add a more efficient path (without test and switch) for cosine power,
   // Basically just a copy past.
   if (a_FilterType != CP_FILTER_TYPE_COSINE_POWER)
   {
   // SL END
   //iterate over cubefaces
   for(iFaceIdx=0; iFaceIdx<6; iFaceIdx++ )
   {
      //if bbox is non empty
      if(a_FilterExtents[iFaceIdx].Empty() == FALSE) 
      {
         uStart = a_FilterExtents[iFaceIdx].m_minCoord[0];
         vStart = a_FilterExtents[iFaceIdx].m_minCoord[1];
         uEnd = a_FilterExtents[iFaceIdx].m_maxCoord[0];
         vEnd = a_FilterExtents[iFaceIdx].m_maxCoord[1];

         normCubeRowStartPtr = a_NormCubeMap[iFaceIdx].m_ImgData + (a_NormCubeMap[iFaceIdx].m_NumChannels * 
            ((vStart * faceWidth) + uStart) );

         srcCubeRowStartPtr = a_SrcCubeMap[iFaceIdx].m_ImgData + (a_SrcCubeMap[iFaceIdx].m_NumChannels * 
            ((vStart * faceWidth) + uStart) );

         //note that <= is used to ensure filter extents always encompass at least one pixel if bbox is non empty
         for(v = vStart; v <= vEnd; v++)
         {
            normCubeRowWalk = 0;
            srcCubeRowWalk = 0;

            for(u = uStart; u <= uEnd; u++)
            {
               //pointer to direction in cube map associated with texel
               texelVect = (normCubeRowStartPtr + normCubeRowWalk);

               //check dot product to see if texel is within cone
               tapDotProd = VM_DOTPROD3(texelVect, a_CenterTapDir);

               if( tapDotProd >= a_DotProdThresh )
               {
                  CP_ITYPE weight;

                  //for now just weight all taps equally, but ideally
                  // weight should be proportional to the solid angle of the tap
                  if(a_bUseSolidAngleWeighting == TRUE)
                  {   //solid angle stored in 4th channel of normalizer/solid angle cube map
                     weight = *(texelVect+3); 
                  }
                  else
                  {   //all taps equally weighted
                     weight = 1.0f;          
                  }

                  switch(a_FilterType)
                  {
                  case CP_FILTER_TYPE_CONE:                                
                  case CP_FILTER_TYPE_ANGULAR_GAUSSIAN:
                     {
                        //weights are in same lookup table for both of these filter types
                        weight *= m_FilterLUT[(int32)(tapDotProd * (m_NumFilterLUTEntries - 1))];
                     }
                     break;
                  case CP_FILTER_TYPE_COSINE:
                     {
                        if(tapDotProd > 0.0f)
                        {
                           weight *= tapDotProd;
                        }
                        else
                        {
                           weight = 0.0f;
                        }
                     }
                     break;
                  case CP_FILTER_TYPE_DISC:
                  default:
                     break;
                  }

                  //iterate over channels
                  for(k=0; k<nSrcChannels; k++)   //(aSrcCubeMap[iFaceIdx].m_NumChannels) //up to 4 channels 
                  {
					 dstAccum[k] += weight * *(srcCubeRowStartPtr + srcCubeRowWalk);
                     srcCubeRowWalk++;                            
                  } 

                  weightAccum += weight; //accumulate weight
               }
               else
               {   
                  //step across source pixel
                  srcCubeRowWalk += nSrcChannels;                    
               }

               normCubeRowWalk += a_NormCubeMap[iFaceIdx].m_NumChannels;
            }

            normCubeRowStartPtr += normCubePitch;
            srcCubeRowStartPtr += srcCubePitch;
         }       
      }
   }
   // SL BEGIN
   }
   else // if (a_FilterType != CP_FILTER_TYPE_COSINE_POWER)
   {
   
   int32 IsPhongBRDF = (a_LightingModel == CP_LIGHTINGMODEL_PHONG_BRDF || a_LightingModel == CP_LIGHTINGMODEL_BLINN_BRDF) ? 1 : 0; // This value will be added to the specular power

   //iterate over cubefaces
   for(iFaceIdx=0; iFaceIdx<6; iFaceIdx++ )
   {
      //if bbox is non empty
      if(a_FilterExtents[iFaceIdx].Empty() == FALSE) 
      {
         uStart = a_FilterExtents[iFaceIdx].m_minCoord[0];
         vStart = a_FilterExtents[iFaceIdx].m_minCoord[1];
         uEnd = a_FilterExtents[iFaceIdx].m_maxCoord[0];
         vEnd = a_FilterExtents[iFaceIdx].m_maxCoord[1];

         normCubeRowStartPtr = a_NormCubeMap[iFaceIdx].m_ImgData + (a_NormCubeMap[iFaceIdx].m_NumChannels * 
            ((vStart * faceWidth) + uStart) );

         srcCubeRowStartPtr = a_SrcCubeMap[iFaceIdx].m_ImgData + (a_SrcCubeMap[iFaceIdx].m_NumChannels * 
            ((vStart * faceWidth) + uStart) );

         //note that <= is used to ensure filter extents always encompass at least one pixel if bbox is non empty
         for(v = vStart; v <= vEnd; v++)
         {
            normCubeRowWalk = 0;
            srcCubeRowWalk = 0;

            for(u = uStart; u <= uEnd; u++)
            {
               //pointer to direction in cube map associated with texel
               texelVect = (normCubeRowStartPtr + normCubeRowWalk);

               //check dot product to see if texel is within cone
               tapDotProd = VM_DOTPROD3(texelVect, a_CenterTapDir);

               if( tapDotProd >= a_DotProdThresh && tapDotProd > 0.0f)
               {
					CP_ITYPE weight;

					//solid angle stored in 4th channel of normalizer/solid angle cube map
					weight = *(texelVect+3); 

					// Here we decide if we use a Phong/Blinn or a Phong/Blinn BRDF.
					// Phong/Blinn BRDF is just the Phong/Blinn model multiply by the cosine of the lambert law
					// so just adding one to specularpower do the trick.					   
					weight *= pow(tapDotProd, (a_SpecularPower + (float32)IsPhongBRDF));

					//iterate over channels
					for(k=0; k<nSrcChannels; k++)   //(aSrcCubeMap[iFaceIdx].m_NumChannels) //up to 4 channels 
					{
						dstAccum[k] += weight * *(srcCubeRowStartPtr + srcCubeRowWalk);
						srcCubeRowWalk++;
					} 

					weightAccum += weight; //accumulate weight
               }
               else
               {   
                  //step across source pixel
                  srcCubeRowWalk += nSrcChannels;                    
               }

               normCubeRowWalk += a_NormCubeMap[iFaceIdx].m_NumChannels;
            }

            normCubeRowStartPtr += normCubePitch;
            srcCubeRowStartPtr += srcCubePitch;
         }       
      }
    }
   } // else // (a_FilterType != CP_FILTER_TYPE_COSINE_POWER)
   // SL END


   //divide through by weights if weight is non zero
   if(weightAccum != 0.0f)
   {
      for(k=0; k<m_NumChannels; k++)
      {
         a_DstVal[k] = (float32)(dstAccum[k] / weightAccum);
      }
   }
   else
   {   //otherwise sample nearest
      CP_ITYPE *texelPtr;

      texelPtr = GetCubeMapTexelPtr(a_CenterTapDir, a_SrcCubeMap);

      for(k=0; k<m_NumChannels; k++)
      {
         a_DstVal[k] = texelPtr[k];
      }
   }
}


//--------------------------------------------------------------------------------------
// Fixup cube edges
//
// average texels on cube map faces across the edges
//--------------------------------------------------------------------------------------
void CCubeMapProcessor::FixupCubeEdges(CImageSurface *a_CubeMap, int32 a_FixupType, int32 a_FixupWidth)
{
   int32 i, j, k;
   int32 face;
   int32 edge;
   int32 neighborFace;
   int32 neighborEdge;

   int32 nChannels = a_CubeMap[0].m_NumChannels;
   int32 size = a_CubeMap[0].m_Width;

   CPCubeMapNeighbor neighborInfo;

   CP_ITYPE* edgeStartPtr;
   CP_ITYPE* neighborEdgeStartPtr;

   int32 edgeWalk;
   int32 neighborEdgeWalk;

   //pointer walk to walk one texel away from edge in perpendicular direction
   int32 edgePerpWalk;
   int32 neighborEdgePerpWalk;

   //number of texels inward towards cubeface center to apply fixup to
   int32 fixupDist;
   int32 iFixup;   

   // note that if functionality to filter across the three texels for each corner, then 
   CP_ITYPE *cornerPtr[8][3];      //indexed by corner and face idx
   CP_ITYPE *faceCornerPtrs[4];    //corner pointers for face
   int32 cornerNumPtrs[8];         //indexed by corner and face idx
   int32 iCorner;                  //corner iterator
   int32 iFace;                    //iterator for faces
   int32 corner;

   //if there is no fixup, or fixup width = 0, do nothing
   if((a_FixupType == CP_FIXUP_NONE) ||
      (a_FixupWidth == 0) 
	  // SL BEGIN
	  || (a_FixupType == CP_FIXUP_BENT && a_CubeMap[0].m_Width != 1) // In case of Bent Fixup and width of 1, we take the average of the texel color.
	  || (a_FixupType == CP_FIXUP_WARP && a_CubeMap[0].m_Width != 1)
	  || (a_FixupType == CP_FIXUP_STRETCH && a_CubeMap[0].m_Width != 1)	  
	  // SL END
	  )
   {
      return;
   }

   //special case 1x1 cubemap, average face colors
   if( a_CubeMap[0].m_Width == 1 )
   {
      //iterate over channels
      for(k=0; k<nChannels; k++)
      {   
         CP_ITYPE accum = 0.0f;

         //iterate over faces to accumulate face colors
         for(iFace=0; iFace<6; iFace++)
         {
            accum += *(a_CubeMap[iFace].m_ImgData + k);
         }

         //compute average over 6 face colors
         accum /= 6.0f;

         //iterate over faces to distribute face colors
         for(iFace=0; iFace<6; iFace++)
         {
            *(a_CubeMap[iFace].m_ImgData + k) = accum;
         }
      }

      return;
   }


   //iterate over corners
   for(iCorner = 0; iCorner < 8; iCorner++ )
   {
      cornerNumPtrs[iCorner] = 0;
   }

   //iterate over faces to collect list of corner texel pointers
   for(iFace=0; iFace<6; iFace++ )
   {
      //the 4 corner pointers for this face
      faceCornerPtrs[0] = a_CubeMap[iFace].m_ImgData;
      faceCornerPtrs[1] = a_CubeMap[iFace].m_ImgData + ( (size - 1) * nChannels );
      faceCornerPtrs[2] = a_CubeMap[iFace].m_ImgData + ( (size) * (size - 1) * nChannels );
      faceCornerPtrs[3] = a_CubeMap[iFace].m_ImgData + ( (((size) * (size - 1)) + (size - 1)) * nChannels );

      //iterate over face corners to collect cube corner pointers
      for(i=0; i<4; i++ )
      {
         corner = sg_CubeCornerList[iFace][i];   
         cornerPtr[corner][ cornerNumPtrs[corner] ] = faceCornerPtrs[i];
         cornerNumPtrs[corner]++;
      }
   }


   //iterate over corners to average across corner tap values
   for(iCorner = 0; iCorner < 8; iCorner++ )
   {
      for(k=0; k<nChannels; k++)
      {             
         CP_ITYPE cornerTapAccum;

         cornerTapAccum = 0.0f;

         //iterate over corner texels and average results
         for(i=0; i<3; i++ )
         {
            cornerTapAccum += *(cornerPtr[iCorner][i] + k);
         }

         //divide by 3 to compute average of corner tap values
         cornerTapAccum *= (1.0f / 3.0f);

         //iterate over corner texels and average results
         for(i=0; i<3; i++ )
         {
            *(cornerPtr[iCorner][i] + k) = cornerTapAccum;
         }
      }
   }   


   //maximum width of fixup region is one half of the cube face size
   fixupDist = VM_MIN( a_FixupWidth, size / 2);

   //iterate over the twelve edges of the cube to average across edges
   for(i=0; i<12; i++)
   {
      face = sg_CubeEdgeList[i][0];
      edge = sg_CubeEdgeList[i][1];

      neighborInfo = sg_CubeNgh[face][edge];
      neighborFace = neighborInfo.m_Face;
      neighborEdge = neighborInfo.m_Edge;

      edgeStartPtr = a_CubeMap[face].m_ImgData;
      neighborEdgeStartPtr = a_CubeMap[neighborFace].m_ImgData;
      edgeWalk = 0;
      neighborEdgeWalk = 0;

      //amount to pointer to sample taps away from cube face
      edgePerpWalk = 0;
      neighborEdgePerpWalk = 0;

      //Determine walking pointers based on edge type
      // e.g. CP_EDGE_LEFT, CP_EDGE_RIGHT, CP_EDGE_TOP, CP_EDGE_BOTTOM
      switch(edge)
      {
         case CP_EDGE_LEFT:
            // no change to faceEdgeStartPtr  
            edgeWalk = nChannels * size;
            edgePerpWalk = nChannels;
         break;
         case CP_EDGE_RIGHT:
            edgeStartPtr += (size - 1) * nChannels;
            edgeWalk = nChannels * size;
            edgePerpWalk = -nChannels;
         break;
         case CP_EDGE_TOP:
            // no change to faceEdgeStartPtr  
            edgeWalk = nChannels;
            edgePerpWalk = nChannels * size;
         break;
         case CP_EDGE_BOTTOM:
            edgeStartPtr += (size) * (size - 1) * nChannels;
            edgeWalk = nChannels;
            edgePerpWalk = -(nChannels * size);
         break;
      }

      //For certain types of edge abutments, the neighbor edge walk needs to 
      //  be flipped: the cases are 
      // if a left   edge mates with a left or bottom  edge on the neighbor
      // if a top    edge mates with a top or right edge on the neighbor
      // if a right  edge mates with a right or top edge on the neighbor
      // if a bottom edge mates with a bottom or left  edge on the neighbor
      //Seeing as the edges are enumerated as follows 
      // left   =0 
      // right  =1 
      // top    =2 
      // bottom =3            
      // 
      //If the edge enums are the same, or the sum of the enums == 3, 
      //  the neighbor edge walk needs to be flipped
      if( (edge == neighborEdge) || ((edge + neighborEdge) == 3) )
      {   //swapped direction neighbor edge walk
         switch(neighborEdge)
         {
            case CP_EDGE_LEFT:  //start at lower left and walk up
               neighborEdgeStartPtr += (size - 1) * (size) *  nChannels;
               neighborEdgeWalk = -(nChannels * size);
               neighborEdgePerpWalk = nChannels;
            break;
            case CP_EDGE_RIGHT: //start at lower right and walk up
               neighborEdgeStartPtr += ((size - 1)*(size) + (size - 1)) * nChannels;
               neighborEdgeWalk = -(nChannels * size);
               neighborEdgePerpWalk = -nChannels;
            break;
            case CP_EDGE_TOP:   //start at upper right and walk left
               neighborEdgeStartPtr += (size - 1) * nChannels;
               neighborEdgeWalk = -nChannels;
               neighborEdgePerpWalk = (nChannels * size);
            break;
            case CP_EDGE_BOTTOM: //start at lower right and walk left
               neighborEdgeStartPtr += ((size - 1)*(size) + (size - 1)) * nChannels;
               neighborEdgeWalk = -nChannels;
               neighborEdgePerpWalk = -(nChannels * size);
            break;
         }            
      }
      else
      { //swapped direction neighbor edge walk
         switch(neighborEdge)
         {
            case CP_EDGE_LEFT: //start at upper left and walk down
               //no change to neighborEdgeStartPtr for this case since it points 
               // to the upper left corner already
               neighborEdgeWalk = nChannels * size;
               neighborEdgePerpWalk = nChannels;
            break;
            case CP_EDGE_RIGHT: //start at upper right and walk down
               neighborEdgeStartPtr += (size - 1) * nChannels;
               neighborEdgeWalk = nChannels * size;
               neighborEdgePerpWalk = -nChannels;
            break;
            case CP_EDGE_TOP:   //start at upper left and walk left
               //no change to neighborEdgeStartPtr for this case since it points 
               // to the upper left corner already
               neighborEdgeWalk = nChannels;
               neighborEdgePerpWalk = (nChannels * size);
            break;
            case CP_EDGE_BOTTOM: //start at lower left and walk left
               neighborEdgeStartPtr += (size) * (size - 1) * nChannels;
               neighborEdgeWalk = nChannels;
               neighborEdgePerpWalk = -(nChannels * size);
            break;
         }
      }


      //Perform edge walk, to average across the 12 edges and smoothly propagate change to 
      //nearby neighborhood

      //step ahead one texel on edge
      edgeStartPtr += edgeWalk;
      neighborEdgeStartPtr += neighborEdgeWalk;

      // note that this loop does not process the corner texels, since they have already been
      //  averaged across faces across earlier
      for(j=1; j<(size - 1); j++)       
      {             
         //for each set of taps along edge, average them
         // and rewrite the results into the edges
         for(k = 0; k<nChannels; k++)
         {             
            CP_ITYPE edgeTap, neighborEdgeTap, avgTap;  //edge tap, neighborEdgeTap and the average of the two
            CP_ITYPE edgeTapDev, neighborEdgeTapDev;

            edgeTap = *(edgeStartPtr + k);
            neighborEdgeTap = *(neighborEdgeStartPtr + k);

            //compute average of tap intensity values
            avgTap = 0.5f * (edgeTap + neighborEdgeTap);

            //propagate average of taps to edge taps
            (*(edgeStartPtr + k)) = avgTap;
            (*(neighborEdgeStartPtr + k)) = avgTap;

            edgeTapDev = edgeTap - avgTap;
            neighborEdgeTapDev = neighborEdgeTap - avgTap;

            //iterate over taps in direction perpendicular to edge, and 
            //  adjust intensity values gradualy to obscure change in intensity values of 
            //  edge averaging.
            for(iFixup = 1; iFixup < fixupDist; iFixup++)
            {
               //fractional amount to apply change in tap intensity along edge to taps 
               //  in a perpendicular direction to edge 
               CP_ITYPE fixupFrac = (CP_ITYPE)(fixupDist - iFixup) / (CP_ITYPE)(fixupDist); 
               CP_ITYPE fixupWeight;

               switch(a_FixupType )
               {
            safeCell<std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>>> cc725b79215d523554a3be4fe50eba63 ���R   �              core::cell::UnsafeCell<bool> 72ef3583eaac9584bbe502ea2a93ea2 �"  �    lock � o   held �V   p           std::sys::windows::mutex::Mutex f4fd0d7c177181ddee5404ecc33f083c �^   �      std::io::stdio::Maybe<std::io::stdio::StdinRaw> 4d5fed2e9b6afdc742b225217f94d13 ��B  r    inner  h  0 buf �� #   @ pos �� #   H cap ��   s          P std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>> be7283e2066c9eb6d717d1c6703884ec ���  0     value R   u           core::cell::UnsafeCell<bool> 72ef3583eaac9584bbe502ea2a93ea2 �R   �              std::io::stdio::Real 4d5fed2e9b6afdc742b225217f94d13::Real ���&  w    RUST$ENCODED$ENUM$0$Fake �^   x  0 std::io::stdio::Maybe<std::io::stdio::StdinRaw> 4d5fed2e9b6afdc742b225217f94d13 ��N   �              std::io::stdio::StdinRaw a96b68ee1197b3962252dabf416ec9d5   z    __0 ��R   {          0 std::io::stdio::Real 4d5fed2e9b6afdc742b225217f94d13::Real ���V   �              std::sys::windows::stdio::Stdin 683465a715b1a826b9b1f65cacf1598 ��  }    __0 ��N   ~          0 std::io::stdio::StdinRaw a96b68ee1197b3962252dabf416ec9d5 z   �              std::sync::mutex::Mutex<std::io::cursor::Cursor<alloc::vec::Vec<u8>>> c6fec1647a00b379b3e6245b7887c8f   �    utf8 �V   �          0 std::sys::windows::stdio::Stdin 683465a715b1a826b9b1f65cacf1598 ��z   �              core::cell::UnsafeCell<std::io::cursor::Cursor<alloc::vec::Vec<u8>>> 2390f4a859cdd894776a5f4de051b3ee 6  e    inner  �   poison ��� �   data �z   �          0 std::sync::mutex::Mutex<std::io::cursor::Cursor<alloc::vec::Vec<u8>>> c6fec1647a00b379b3e6245b7887c8f b   �              std::io::cursor::Cursor<alloc::vec::Vec<u8>> 7285a47f05c96b206239c3ce75df3270   �    value z   �            core::cell::UnsafeCell<std::io::cursor::Cursor<alloc::vec::Vec<u8>>> 2390f4a859cdd894776a5f4de051b3ee "  5    inner  #    pos ��b   �            std::io::cursor::Cursor<alloc::vec::Vec<u8>> 7285a47f05c96b206239c3ce75df3270 �   �              alloc::arc::Arc<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>> fd25c479ff52d9c16365719b510fc2c3 
 �      �    __0 ���   �           core::nonzero::NonZero<const alloc::arc::Arc<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>>*> deed123032d91cef3128fbb6ce1f189d ��   �              core::ptr::Shared<alloc::arc::ArcInner<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>>> e19c0d917be852e8544668d6e4c7601a �   �              core::marker::PhantomData<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>> 2730971e6f1f366944fa163d8744a2be ��&  �    ptr �� �    phantom ��   �           alloc::arc::Arc<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>> fd25c479ff52d9c16365719b510fc2c3 �   �              core::nonzero::NonZero<const alloc::arc::ArcInner<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>>*> d8668b8af281f40ce64e67e8fb6bb18 �  �    pointer ���   �           core::ptr::Shared<alloc::arc::ArcInner<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>>> e19c0d917be852e8544668d6e4c7601a �                core::marker::PhantomData<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>> 2730971e6f1f366944fa163d8744a2be ��   �              alloc::arc::ArcInner<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>> 2b1536b0f2f42b2cf52d57b8fb5705ba ���
 �      �    __0 ���   �           core::nonzero::NonZero<const alloc::arc::ArcInner<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>>*> d8668b8af281f40ce64e67e8fb6bb18 �   �              std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>> 56d2b1015618914e29a5724a82a0074 ��6  �    strong ��� �   weak � �   data �   �          0 alloc::arc::ArcInner<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>> 2b1536b0f2f42b2cf52d57b8fb5705ba ���~   �              core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>> df5d073db6c0bc70a1925fd57b0a6015 ���6  �    inner  �   poison ��� �   data �   �            std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>> 56d2b1015618914e29a5724a82a0074 ��~   �              core::cell::UnsafeCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>> 6a90097359984c4ab866a2dea93074ca &  �    borrow ��� �   value ~   �           core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>> df5d073db6c0bc70a1925fd57b0a6015 ���  �    value ~   �           core::cell::UnsafeCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>> 6a90097359984c4ab866a2dea93074ca �   �              alloc::btree::node::LeafNode<std::sys::windows::process::WindowsEnvKey, core::option::Option<std::ffi::os_str::OsString>> 3609dc65ec350305c30c363a9ec4765e ���
 �      �    __0 ���   �           core::nonzero::NonZero<const alloc::btree::node::LeafNode<std::sys::windows::process::WindowsEnvKey, core::option::Option<std::ffi::os_str::OsString>>*> 4cdd9a4e7297e35caa5393c5d242515c ^   �      core::option::Option<std::ffi::os_str::OsString> 534e71578096db3b5b502fabe5645a40  �  #    �   �              alloc::btree::node::InternalNode<std::sys::windows::process::WindowsEnvKey, core::option::Option<std::ffi::os_str::OsString>> ab25933b7a89f951e4cc68a57c0cc21f ���
 �    ^  �    keys � �  vals � �  parent ��� !   parent_idx ��� !   len ��   �           alloc::btree::node::LeafNode<std::sys::windows::process::WindowsEnvKey, core::option::Option<std::ffi::os_str::OsString>> 3609dc65ec350305c30c363a9ec4765e ���N   �              core::option::Some 534e71578096db3b5b502fabe5645a40::Some &  �    RUST$ENCODED$ENUM$0$None �^   �   core::option::Option<std::ffi::os_str::OsString> 534e71578096db3b5b502fabe5645a40 �   �              alloc::btree::node::BoxedNode<std::sys::windows::process::WindowsEnvKey, core::option::Option<std::ffi::os_str::OsString>> f0a307fd9b4d8bba814bf1c80d5012a4 �� �  #   `  �"  �    data � �   edges �   �          �alloc::btree::node::InternalNode<std::sys::windows::process::WindowsEnvKey, core::option::Option<std::ffi::os_str::OsString>> ab25933b7a89f951e4cc68a57c0cc21f ���N   �           core::option::Some 534e71578096db3b5b502fabe5645a40::Some �   �              core::ptr::Unique<alloc::btree::node::LeafNode<std::sys::windows::process::WindowsEnvKey, core::option::Option<std::ffi::os_str::OsString>>> 1a4d81b58c403d3c7b3b1ed40d8ef76c   �    ptr ��   �           alloc::btree::node::BoxedNode<std::sys::windows::process::WindowsEnvKey, core::option::Option<std::ffi::os_str::OsString>> f0a307fd9b4d8bba814bf1c80d5012a4 ���   �              core::nonzero::NonZero<const alloc::btree::node::LeafNode<std::sys::windows::process::WindowsEnvKey, core::option::Option<std::ffi::os_str::OsString>>*> 4cdd9a4e7297e35caa5393c5d242515c �   �              core::marker::PhantomData<alloc::btree::node::LeafNode<std::sys::windows::process::WindowsEnvKey, core::option::Option<std::ffi::os_str::OsString>>> 724486c629214aa3b6877effcee4c9fe *  �    pointer �� �    _marker ���   �           core::ptr::Unique<alloc::btree::node::LeafNode<std::sys::windows::process::WindowsEnvKey, core::option::Option<std::ffi::os_str::OsString>>> 1a4d81b58c403d3c7b3b1ed40d8ef76c �                core::marker::PhantomData<alloc::btree::node::LeafNode<std::sys::windows::process::WindowsEnvKey, core::option::Option<std::ffi::os_str::OsString>>> 724486c629214aa3b6877effcee4c9fe F   �              const slice<u8>* 1471e85eabbff499a6a1473b723d185c   �    __0 ��^   �           core::nonzero::NonZero<const slice<u8>*> b6d8ba7d66884d50f5ded7307de17da �F   H           const slice<u8>* 1471e85eabbff499a6a1473b723d185c F   �              std::path::Path 2e317d78d1e73779d0700318c062e291 �
 �  
    �    __0 ��f   �           core::nonzero::NonZero<const std::path::Path*> c73f3ecc43f8710deb7a8b47c0c846ed ��J   �      std::net::addr::SocketAddr 2e1e585879ba9d2d67f8bb4485675209 ��
 �      �    __0 ��r   �           core::nonzero::NonZero<const std::net::addr::SocketAddr*> 3ed2e2bf5419f516993ae7aca0567d1c ���    V4 ���  V6 ���*    u   �  std::net::addr::SocketAddr �R   �              std::net::addr::SocketAddrV4 bad8b02b99c362909548b99cbf572f27 R   �              std::net::addr::SocketAddrV6 16e88d50875c70bea312c7725cd0e142 Z  �    RUST$ENUM$DISR ��� �   __0 �� �    RUST$ENUM$DISR ��� �   __0 ��J   �    std::net::addr::SocketAddr 2e1e585879ba9d2d67f8bb4485675209 ��Z   �              std::sys::windows::c::sockaddr_in 6740f5c1c40bdf764c275384786a2df1 ���  �    inner R   �           std::net::addr::SocketAddrV4 bad8b02b99c362909548b99cbf572f27 Z   �              std::sys::windows::c::sockaddr_in6 95b306647d0d06abf048c2657af0675b ��  �    inner R   �           std::net::addr::SocketAddrV6 16e88d50875c70bea312c7725cd0e142 V   �              std::sys::windows::c::in_addr 8a39fa2b6a50165b595f4909ce4ddfc9 ���    #     �V  !     sin_family ��� !    sin_port � �   sin_addr � �   sin_zero �Z   �           std::sys::windows::c::sockaddr_in 6740f5c1c40bdf764c275384786a2df1 ���V   �              std::sys::windows::c::in6_addr 1abcf21c003942b97fcac05677f4383e ��r  !     sin6_family �� !    sin6_port  u    sin6_flowinfo  �   sin6_addr  u    sin6_scope_id Z   �           std::sys::windows::c::sockaddr_in6 95b306647d0d06abf048c2657af0675b ��  u     s_addr ���V   �           std::sys::windows::c::in_addr 8a39fa2b6a50165b595f4909ce4ddfc9 ���     #     �  �    s6_addr ��V   �           std::sys::windows::c::in6_addr 1abcf21c003942b97fcac05677f4383e ��^   �              alloc::arc::ArcInner<std::thread::Inner> f1a79751bbf833edf7bba42d12697350 
 �      �    __0 ��~   �           core::nonzero::NonZero<const alloc::arc::ArcInner<std::thread::Inner>*> f707bf46db282209e2bae4625e4676ae �J   �              std::thread::Inner 883a6ffd2961da2bf831d3c46ebf95c1 ��6  �    strong ��� �   weak � �   data �^   �          P alloc::arc::ArcInner<std::thread::Inner> f1a79751bbf833edf7bba42d12697350 ^   �      core::option::Option<std::ffi::c_str::CString> eb433c633af9a58a3d8808cbffe174db ��N   �              std::thread::ThreadId fd74df377b9fb465afce690a4d878f83 ���R   �              std::sync::mutex::Mutex<()> 65c873cb0a6594689bfd9f646e60406a �R   �              std::sync::condvar::Condvar a42c43abbf109530fc58676b8ea82f9f �R  �    name � �   id ��� �   state  �    lock � �  0 cvar �J   �          @ std::thread::Inner 883a6ffd2961da2bf831d3c46ebf95c1 ��N   �              core::option::Some eb433c633af9a58a3d8808cbffe174db::Some &  �    RUST$ENCODED$ENUM$0$None �^   �   core::option::Option<std::ffi::c_str::CString> eb433c633af9a58a3d8808cbffe174db ��  #     __0 ��N   �           std::thread::ThreadId fd74df377b9fb465afce690a4d878f83 ���R   �              core::cell::UnsafeCell<()> 4ddb34b9033c17dd886ac5c65b05c165 ��6  e    inner  �   poison ��� �  	 data �R   �           std::sync::mutex::Mutex<()> 65c873cb0a6594689bfd9f646e60406a �Z   �              std::sys_common::condvar::Condvar bbd7ae0b3ac8aea86dfc840e8e9e9c06 ���
 �    "  �    inner  �   mutex R   �           std::sync::condvar::Condvar a42c43abbf109530fc58676b8ea82f9f �  l    __0 ��N   �           core::option::Some eb433c633af9a58a3d8808cbffe174db::Some         value R   �            core::cell::UnsafeCell<()> 4ddb34b9033c17dd886ac5c65b05c165 ��Z   �              std::sys::windows::condvar::Condvar e34adae2fe8b8421536e5195531bd9e9 �      __0 ��Z              std::sys_common::condvar::Condvar bbd7ae0b3ac8aea86dfc840e8e9e9c06 ���v   �              core::cell::UnsafeCell<std::sys::windows::c::CONDITION_VARIABLE> cc0e09dd4f123a5a80303a53f4b32e2b       inner Z              std::sys::windows::condvar::Condvar e34adae2fe8b8421536e5195531bd9e9 �^   �              std::sys::windows::c::CONDITION_VARIABLE 4dbf6cbc6eb178b1a8ae88b3b2b0271f       value v              core::cell::UnsafeCell<std::sys::windows::c::CONDITION_VARIABLE> cc0e09dd4f123a5a80303a53f4b32e2b       ptr ��^   
           std::sys::windows::c::CONDITION_VARIABLE 4dbf6cbc6eb178b1a8ae88b3b2b0271f   �    __0 ��              core::nonzero::NonZero<const alloc::btree::node::InternalNode<std::sys::windows::process::WindowsEnvKey, std::ffi::os_str::OsString>*> 566842374d27220abc89069084702050 ��n   �              alloc::arc::ArcInner<std::sync::mpsc::blocking::Inner> 5e713c68eb0d2b4b22eb59b4082f76cf ��
           __0 ��              core::nonzero::NonZero<const alloc::arc::ArcInner<std::sync::mpsc::blocking::Inner>*> 831f8ae4cf322c41d5e09cd2b7064664 ���V   �              std::sync::mpsc::blocking::Inner 2f046a4597f3c982d5e4b7d205cae2a5 6  �    strong ��� �   weak �    data �n               alloc::arc::ArcInner<std::sync::mpsc::blocking::Inner> 5e713c68eb0d2b4b22eb59b4082f76cf ��J   �              std::thread::Thread 92d83434dacdb854659117cf76cbd1c8 �&      thread ��� �   woken V              std::sync::mpsc::blocking::Inner 2f046a4597f3c982d5e4b7d205cae2a5 Z   �              alloc::arc::Arc<std::thread::Inner> e89935fea312a2792d0c2690ba278749 �      inner J              std::thread::Thread 92d83434dacdb854659117cf76cbd1c8 �r   �              core::ptr::Shared<alloc::arc::ArcInner<std::thread::Inner>> 9b34687c52329f1d17119be6b78a8c49 �f   �              core::marker::PhantomData<std::thread::Inner> 6106b9b08e565e467331bdc77580c5b6 ���&      ptr ��     phantom ��Z              alloc::arc::Arc<std::thread::Inner> e89935fea312a2792d0c2690ba278749 �~   �              core::nonzero::NonZero<const alloc::arc::ArcInner<std::thread::Inner>*> f707bf46db282209e2bae4625e4676ae �      pointer ��r               core::ptr::Shared<alloc::arc::ArcInner<std::thread::Inner>> 9b34687c52329f1d17119be6b78a8c49 �f                core::marker::PhantomData<std::thread::Inner> 6106b9b08e565e467331bdc77580c5b6 ���r   �              core::ops::function::const Fn<(std::panicking::PanicInfo*)>* 662d9ebe37634b1b84eb0e4e44ac1243   #    __0 ��v   $           core::nonzero::NonZero<const Fn<(std::panicking::PanicInfo*)>*> f21d4a911ec1c8b3ef79f67f365f2612 �r   c           core::ops::function::const Fn<(std::panicking::PanicInfo*)>* 662d9ebe37634b1b84eb0e4e44ac1243 
 J  
    '    __0 ��n   (           core::nonzero::NonZero<const std::ffi::os_str::OsStr*> a77d4cd22ac61f5decf114a133261728 ��
 M  
    *    __0 ��v   +           core::nonzero::NonZero<const std::sys::windows::os_str::Slice*> 7a60db290485e0f2b9d2a7620e3d8824 �Z   �              std::sys::windows::handle::Handle 8d1b3a18de397f80be8faadbe3d0f86d ���  -    handle ���Z   .           std::sys::windows::thread::Thread 76fd78be155755fbb870d099c05e7489 ���Z   �              std::sys::windows::handle::RawHandle 9ce1b3771ed5bad8da64daf1ab5a5f1b   0    __0 ��Z   1           std::sys::windows::handle::Handle 8d1b3a18de397f80be8faadbe3d0f86d ��� 	   K                j   �              core::nonzero::NonZero<const alloc::vec::Vec<u8>*> d1ebee4fb30c8b4880ba040037083653 �� 	   4                 	                   r   �              core::nonzero::NonZero<const std::ffi::os_str::OsString*> dbfdb8f50a0046fcb6f8bbdf23350ee7 ��� 	   7                 	                    	   %                �   �              core::nonzero::NonZero<const alloc::vec::Vec<alloc::boxed::Box<FnBox<()>>>*> 37d4fb53fd95957e1e5e56a389afd473  	   ;                j   �              core::nonzero::NonZero<const std::ffi::c_str::CStr*> 6001d0a5c79d8e6d8775fcd820630534  	   =                �   �              core::nonzero::NonZero<const alloc::vec::Vec<(mut u8*, unsafe extern "C" fn(mut u8*))>*> 4aa0b9ab07d9c6924385aa714f4eb3c7  	   ?                ~   �              core::nonzero::NonZero<const alloc::arc::ArcInner<std::path::PathBuf>*> 1a3ea747d17157f6c713bab791a5eba8 � 	   A                �   �              core::nonzero::NonZero<const alloc::arc::Arc<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>>>*> 22c1cfc6e5fa886a78652a18f58deca1 �� 	   C                ~   �              core::nonzero::NonZero<const std::sys::windows::mutex::ReentrantMutex*> 58d180e3272f8ef24b64adce03f186d4 � 	   E                 	                    	   ,                �   �              core::nonzero::NonZero<const alloc::arc::Arc<std::sync::mutex::Mutex<std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>>>>*> 747447379580ffcd4c8bb99e3fbdc818 �� 	   I                �   �              core::nonzero::NonZero<const alloc::arc::Arc<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>>*> deed123032d91cef3128fbb6ce1f189d � 	   K                 	   �                 	   �                ^   �              core::nonzero::NonZero<const slice<u8>*> b6d8ba7d66884d50f5ded7307de17da � 	   O                f   �              core::nonzero::NonZero<const std::path::Path*> c73f3ecc43f8710deb7a8b47c0c846ed �� 	   Q                r   �              core::nonzero::NonZero<const std::net::addr::SocketAddr*> 3ed2e2bf5419f516993ae7aca0567d1c ��� 	   S                 	                   �   �              core::nonzero::NonZero<const alloc::btree::node::InternalNode<std::sys::windows::process::WindowsEnvKey, std::ffi::os_str::OsString>*> 566842374d27220abc89069084702050 �� 	   V                �   �              core::nonzero::NonZero<const alloc::arc::ArcInner<std::sync::mpsc::blocking::Inner>*> 831f8ae4cf322c41d5e09cd2b7064664 ��� 	   X                v   �              core::nonzero::NonZero<const Fn<(std::panicking::PanicInfo*)>*> f21d4a911ec1c8b3ef79f67f365f2612 � 	   Z                n   �              core::nonzero::NonZero<const std::ffi::os_str::OsStr*> a77d4cd22ac61f5decf114a133261728 �� 	   \                 	   Y                v   �              core::nonzero::NonZero<const std::sys::windows::os_str::Slice*> 7a60db290485e0f2b9d2a7620e3d8824 � 	   _                 	   e                 	   0                 	   8                 	   M                 	   ,                 	   Q                 	   2                f   �      core::result::Result<mut u8*, alloc::allocator::AllocErr> 169e409c457a6e1810724f7a7106524  	   h                 	   �                 	   �                z   �      core::result::Result<alloc::string::String, std::sys::windows::os_str::Buf> 358baf56d8abb6dc2547c627c9033279 � 	   l                 	   5                 	   J                n   �              std::thread::local::LocalKey<core::cell::Cell<usize>> c3ded3bfb04cd6269f4ce0251acec021 ��� 	   p                 	   �                j   �      core::result::Result<usize, std::thread::local::AccessError> 6835d8cb61eaed538189a97e4eddcd7a  	   s                N   �              std::sync::once::Once 8ce7b4940b8225ddf7e1453fe4dff291 ��� 	   u                 	   �                b   �      core::result::Result<i32, alloc::boxed::Box<Any>> 17500d065cd15157ead4f97c85ac021a ��� 	   x                J   �              alloc::vec::Vec<u16> 1ce35a9ee65957a1ead5cd20e28e29f4  	   z                 	   l                z   �      core::result::Result<std::ffi::c_str::CString, std::ffi::c_str::NulError> 9d90da45f277ad05e682f3d0a73645b3 ��� 	   }                 	   �                 	                   J   �      core::option::Option<u64> a6f44d45164f082d1d6413aca94b4478 ��� 	   �                N   �              std::io::error::Error a625ef5ebf00dc4af82cc94a060fdfe6 ��� 	   �                J   �              core::fmt::Arguments 7d50eb09db88397a63ccad989d6c5f84  	   �                Z   �              std::sys::windows::thread::Thread 76fd78be155755fbb870d099c05e7489 ��� 	   �                 	                   n   �              core::marker::PhantomData<mut std::sync::once::Waiter*> 9b902f0409967c6fb49e28f7a32d9bca �&  �    state  �    _marker ��N   �           std::sync::once::Once 8ce7b4940b8225ddf7e1453fe4dff291 ���Z   �              std::sys::windows::rwlock::RWLock b3f8ed9a138d88138898321f11dc4091 ���  �    __0 ��V   �           std::sys_common::rwlock::RWLock 98023bfd6f03b3187022955207ae3b5c �V   �              std::panicking::Custom 35dc93575a05756e62d3813b085f9a11::Custom ��*  �    RUST$ENCODED$ENUM$0$Default ��B   �   std::panicking::Hook 35dc93575a05756e62d3813b085f9a11 �   �              core::cell::UnsafeCell<core::option::Option<core::cell::RefCell<core::option::Option<alloc::boxed::Box<Write>>>>> 7b4085efc11765f8a5743e24120556 �N   �              core::cell::Cell<bool> 8bdd0bbecd4881831b5e94cc67423f4 ���F  �    inner  �    dtor_registered �� �  ! dtor_running �   �          ( std::thread::local::fast::Key<core::cell::RefCell<core::option::Option<alloc::boxed::Box<Write>>>> 90153b15a31e74f8c13b922001cdc44a ��~   �              core::cell::UnsafeCell<core::option::Option<core::cell::Cell<usize>>> 829cffb3f97539fd5bb67a6dac198278 ���F  �    inner  �   dtor_registered �� �   dtor_running �n   �           std::thread::local::fast::Key<core::cell::Cell<usize>> d80026f06fb0401f4f096b5f3281de4b ��B   �      std::io::error::Repr 5de7b60e6bb97f0274742871b734c574   �    repr �N   �           std::io::error::Error a625ef5ebf00dc4af82cc94a060fdfe6 ���.    Os ���  Simple ���  Custom ���&        �  std::io::error::Repr ���~   NotFound �  PermissionDenied �  ConnectionRefused   ConnectionReset ��  ConnectionAborted   NotConnected �  AddrInUse   AddrNotAvailable �  BrokenPipe ��� 	 AlreadyExists  
 WouldBlock ���  InvalidInput �  InvalidData ��  TimedOut �  WriteZero   Interrupted ��  Other   UnexpectedEof   __Nonexhaustive ��*        �  std::io::error::ErrorKind ��N   �              std::io::error::Custom 846c1f9d0ed6e867c3082dea612b72ec ��
 �    �  �    RUST$ENUM$DISR ��� t    __0 �� �    RUST$ENUM$DISR ��� �   __0 �� �    RUST$ENUM$DISR ��� �   __0 ��B   �   std::io::error::Repr 5de7b60e6bb97f0274742871b734c574 N   �              std::error::Box<Error> 4b02bcacf80967977b1d81cf67587381 ��"  �   kind � �    error N   �           std::io::error::Custom 846c1f9d0ed6e867c3082dea612b72ec ��N   c           std::error::Box<Error> 4b02bcacf80967977b1d81cf67587381 ��Z  _    RUST$ENUM$DISR ��� t    __0 �� _    RUST$ENUM$DISR ��� �   __0 ��^   �   core::result::Result<i32, std::io::error::Error> 3d51faf45e402282e76e8ba9fce10f5d V                std::sys::windows::stdio::Stderr 47b353cd5daf60b14657275f8d088052 N   �              core::option::Some 7bb5b91e9f396552597c341583338ee3::Some &  �    RUST$ENCODED$ENUM$0$None �Z   �   core::option::Option<std::thread::Thread> 7bb5b91e9f396552597c341583338ee3 ���      __0 ��N   �           core::option::Some 7bb5b91e9f396552597c341583338ee3::Some n                core::marker::PhantomData<mut std::sync::once::Waiter*> 9b902f0409967c6fb49e28f7a32d9bca �n   �              core::cell::UnsafeCell<std::sys::windows::c::SRWLOCK> ff3a743c3fa73be281f2e4fe74687e1b ���  �    inner Z   �           std::sys::windows::rwlock::RWLock b3f8ed9a138d88138898321f11dc4091 ���R   �              std::sys::windows::c::SRWLOCK 49c8c035d3915d12dc0c9c3f8318dfc   �    value n   �           core::cell::UnsafeCell<std::sys::windows::c::SRWLOCK> ff3a743c3fa73be281f2e4fe74687e1b ���R   
           std::sys::windows::c::SRWLOCK 49c8c035d3915d12dc0c9c3f8318dfc N   �              core::option::Some 41e8ff92547ce4d6775c06bc2ff891fb::Some &  �    RUST$ENCODED$ENUM$0$None �Z   �   core::option::Option<std::thread::Thread*> 41e8ff92547ce4d6775c06bc2ff891fb ��
       �    __0 ��N   �           core::option::Some 41e8ff92547ce4d6775c06bc2ff891fb::Some N   �              core::option::Some e04268e72ad002583066f38cf610ce20::Some &  �    RUST$ENCODED$ENUM$0$None �J   �   core::option::Option<str*> e04268e72ad002583066f38cf610ce20 ��  C    __0 ��N   �           core::option::Some e04268e72ad002583066f38cf610ce20::Some ~   �              core::cell::UnsafeCell<core::option::Option<alloc::boxed::Box<Write>>> e20b305bda12c5107bf7e6877c5f9649 ��&  �    borrow ��� �   value z   �           core::cell::RefCell<core::option::Option<alloc::boxed::Box<Write>>> 90fab8042230a387c5baf426d274137f �^   �      core::option::Option<alloc::boxed::Box<Write>> e6e82c7cae7df908a7e0bf196d8bb5ec ��  �    value ~   �           core::cell::UnsafeCell<core::option::Option<alloc::boxed::Box<Write>>> e20b305bda12c5107bf7e6877c5f9649 ��N   �              core::option::Some e6e82c7cae7df908a7e0bf196d8bb5ec::Some &  �    RUST$ENCODED$ENUM$0$None �^   �   core::option::Option<alloc::boxed::Box<Write>> e6e82c7cae7df908a7e0bf196d8bb5ec ��J   �              std::io::Box<Write> 7dd43f69b2f1a32851dccff4fd71e786 �  �    __0 ��N   �           core::option::Some e6e82c7cae7df908a7e0bf196d8bb5ec::Some J   c           std::io::Box<Write> 7dd43f69b2f1a32851dccff4fd71e786 �   �      core::option::Option<core::cell::RefCell<core::option::Option<alloc::boxed::Box<Write>>>> 968ab7a0f29e1da5368397aa642b121c ���  �    value �   �            core::cell::UnsafeCell<core::option::Option<core::cell::RefCell<core::option::Option<alloc::boxed::Box<Write>>>>> 7b4085efc11765f8a5743e24120556 �  o    value N   �           core::cell::Cell<bool> 8bdd0bbecd4881831b5e94cc67423f4 ���z   �              core::cell::RefCell<core::option::Option<alloc::boxed::Box<Write>>> 90fab8042230a387c5baf426d274137f �J  s    RUST$ENUM$DISR ��� s    RUST$ENUM$DISR ��� �   __0 ��   �    core::option::Option<core::cell::RefCell<core::option::Option<alloc::boxed::Box<Write>>>> 968ab7a0f29e1da5368397aa642b121c ���  0     poisoned �R   �           std::sync::once::OnceState 6c57e57f4902cd12f971e4a6c3128b7e ��F   �              core::any::Any* 1701ce99a25fe19dba51f56041db156a �N   �              std::panicking::Location 5b42c403aca7f6017b142b511bdc3f1c *  �    payload �� �   location �R   �          ( std::panicking::PanicInfo 686a7716ff9aea01d93c6abe3f67cfb4 ���F   c           core::any::Any* 1701ce99a25fe19dba51f56041db156a �2  C    file � u    line � u    col ��N   �           std::panicking::Location 5b42c403aca7f6017b142b511bdc3f1c r   �              core::ops::function::mut Fn<(std::panicking::PanicInfo*)>* 99387e097ccbadcf347d5bf008e809f8 ��  �    __0 ��V   �           std::panicking::Custom 35dc93575a05756e62d3813b085f9a11::Custom ��r   c           core::ops::function::mut Fn<(std::panicking::PanicInfo*)>* 99387e097ccbadcf347d5bf008e809f8 ��^   �      core::option::Option<core::cell::Cell<usize>> 7a2d0c226ec68c8b263fd1a7e6357e09 ���  �    value ~   �           core::cell::UnsafeCell<core::option::Option<core::cell::Cell<usize>>> 829cffb3f97539fd5bb67a6dac198278 ���J  s    RUST$ENUM$DISR ��� s    RUST$ENUM$DISR ��� �   __0 ��^   �   core::option::Option<core::cell::Cell<usize>> 7a2d0c226ec68c8b263fd1a7e6357e09 ���V   �              core::fmt::builders::DebugList 67b3bb821acdf3c82b8f93849bf1d4c5 �� 	   �                 	   �                &        r  core::option::Option ��� 	   �                ^   �      core::result::Result<i32, std::io::error::Error> 3d51faf45e402282e76e8ba9fce10f5d  	   �                 	                    	                    	   P                V   �              std::sys::windows::stdio::Stderr 47b353cd5daf60b14657275f8d088052  	   �                ^   �              std::sys_common::thread_info::ThreadInfo 10b4d613cfe1fb55c1e1ad42a736cf6f  	   �                Z   �      core::option::Option<std::thread::Thread> 7bb5b91e9f396552597c341583338ee3 ��� 	   �                 	   �                R   �              std::sync::once::OnceState 6c57e57f4902cd12f971e4a6c3128b7e �� 	   �                V   �              std::sys_common::rwlock::RWLock 98023bfd6f03b3187022955207ae3b5c � 	   �                 	   �                R   �              std::panicking::PanicInfo 686a7716ff9aea01d93c6abe3f67cfb4 ��� 	                    	   �                Z   �      core::option::Option<std::thread::Thread*> 41e8ff92547ce4d6775c06bc2ff891fb �� 	                    	                    	   �                ^   �      core::option::Option<std::ffi::c_str::CString*> df6efb664fffb740ac838fee77158e12 � 	                   Z   �      core::option::Option<std::ffi::c_str::CStr*> ecd92a64d641eaf56bb51e5314d954c � 	   
                J   �      core::option::Option<str*> e04268e72ad002583066f38cf610ce20 �� 	                    	   �                �   �              std::thread::local::fast::Key<core::cell::RefCell<core::option::Option<alloc::boxed::Box<Write>>>> 90153b15a31e74f8c13b922001cdc44a �� 	                    	   �                �   �              core::cell::Cell<mut alloc::arc::Arc<std::sync::mutex::Mutex<std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>>>>*> 5884988de0cffe24c994aad03a31fff9 �� Q         
     2  d    lock �    ptr ��    init �               std::io::lazy::Lazy<std::sync::mutex::Mutex<std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>>>> 40f0aeb92c1e1752bd7e275b8e1dbe37 ��   �              core::cell::Cell<mut alloc::arc::Arc<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>>>*> afa5c823f8d1f79c48a6d79b3ed96136 �� C         
     2  d    lock �    ptr ��    init ��               std::io::lazy::Lazy<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>>> 3eae2d2a6547a5548c6d5cb5315c900c ��   �              core::cell::Cell<mut alloc::arc::Arc<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>>*> d2bede921551a2c77f979b46c226835a � �         
     2  d    lock �    ptr ��    init �               std::io::lazy::Lazy<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>> 4511971c7069bed256db2bf3271ee21b N   �              core::option::Some 4b7eef69e1db81d9e81e9a5cff61ca84::Some &  !    RUST$ENCODED$ENUM$0$None �   "   core::option::Option<alloc::arc::Arc<std::sync::mutex::Mutex<std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>>>>> 4b7eef69e1db81d9e81e9a5cff61ca84 ���  Q    __0 ��N   $           core::option::Some 4b7eef69e1db81d9e81e9a5cff61ca84::Some Z  <    RUST$ENUM$DISR ��� }   __0 �� <    RUST$ENUM$DISR ��� �   __0 ��z   &  8 core::result::Result<std::sys::windows::stdio::Stdin, std::io::error::Error> 1fc912ef261dc612fbb035ee786fae5e 
 a    V   �              std::sys_common::poison::Guard fd7d2f4207398ff05affa27abe953dce ��*  (    __lock ��� )   __poison �   *           std::sync::mutex::MutexGuard<std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>>> 21023cbbeaffd6f71f16e6f25849319c �  0     panicking V   ,           std::sys_common::poison::Guard fd7d2f4207398ff05affa27abe953dce ��   �              std::sync::mutex::MutexGuard<std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>>> 21023cbbeaffd6f71f16e6f25849319c ��   �              std::sys_common::poison::PoisonError<std::sync::mutex::MutexGuard<std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>>>> f8c655a13d7fff19c6c56c4c8052726a ���Z  <    RUST$ENUM$DISR ��� .   __0 �� <    RUST$ENUM$DISR ��� /   __0 ��B  0   core::result::Result<std::sync::mutex::MutexGuard<std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>>>, std::sys_common::poison::PoisonError<std::sync::mutex::MutexGuard<std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>>>>> 563eea56864d46f054fba5ffc26605e8   .    guard �   2           std::sys_common::poison::PoisonError<std::sync::mutex::MutexGuard<std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>>>> f8c655a13d7fff19c6c56c4c8052726a ���  Q    inner N   4           std::io::stdio::Stdin 89eea673a2ab9daff7e29a802ab160b7 ���N   �              core::option::Some 7a35e223246898c13b118ae8958a026b::Some &  6    RUST$ENCODED$ENUM$0$None ��   7   core::option::Option<alloc::arc::Arc<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>>>> 7a35e223246898c13b118ae8958a026b ���  C    __0 ��N   9           core::option::Some 7a35e223246898c13b118ae8958a026b::Some 
 �    *  ;    __lock ��� )   __poison ��   <           std::sys_common::remutex::ReentrantMutexGuard<core::cell::RefCell<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>> 8fc2e87a48aa663ddacbf392748fb99f ��   �              std::sys_common::remutex::ReentrantMutexGuard<core::cell::RefCell<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>> 8fc2e87a48aa663ddacbf392748fb99f ��   �              std::sys_common::poison::PoisonError<std::sys_common::remutex::ReentrantMutexGuard<core::cell::RefCell<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>>> 435d56b2af94a5215d3b27fe1f633a59 ���Z  <    RUST$ENUM$DISR ��� >   __0 �� <    RUST$ENUM$DISR ��� ?   __0 ��  @   core::result::Result<std::sys_common::remutex::ReentrantMutexGuard<core::cell::RefCell<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>>, std::sys_common::poison::PoisonError<std::sys_common::remutex::ReentrantMutexGuard<core::cell::RefCell<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>>>> 822e72f7a26a4d92bdc9bc450530ca9b   >    guard �   B           std::sys_common::poison::PoisonError<std::sys_common::remutex::ReentrantMutexGuard<core::cell::RefCell<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>>> 435d56b2af94a5215d3b27fe1f633a59 ���  C    inner N   D           std::io::stdio::Stdout 8cf2e64e7bf5ad51abb3d24bb391a9c1 ��
 �      F    borrow ���N   G           core::cell::BorrowRefMut 945aad95c0a49b0bbb2b8da870ab525b J   �              core::result::Ok b4ab634dd5f3700238106669c5093f7a::Ok &  I    RUST$ENCODED$ENUM$0$Err ���   J   core::result::Result<core::cell::RefMut<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>, core::cell::BorrowMutError> b4ab634dd5f3700238106669c5093f7a ���   �              core::cell::RefMut<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>> 3593c77b25b154a28c30e70585fe22c7 �  L    __0 ��J   M           core::result::Ok b4ab634dd5f3700238106669c5093f7a::Ok 
 �    N   �              core::cell::BorrowRefMut 945aad95c0a49b0bbb2b8da870ab525b &  O    value  P   borrow ���   Q           core::cell::RefMut<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>> 3593c77b25b154a28c30e70585fe22c7 �N   �              core::result::Err 92cb5263dae4acea8423ca61a72fe920::Err ��&  S    RUST$ENCODED$ENUM$0$Ok ���^   T   core::result::Result<(), std::io::error::Error> 92cb5263dae4acea8423ca61a72fe920 �  �    __0 ��N   V           core::result::Err 92cb5263dae4acea8423ca61a72fe920::Err ��N   �              core::option::Some 9dbd5ce22e90bfa2e9a6f17732390c8b::Some &  X    RUST$ENCODED$ENUM$0$None ��   Y   core::option::Option<alloc::arc::Arc<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>>> 9dbd5ce22e90bfa2e9a6f17732390c8b ��  �    __0 ��N   [           core::option::Some 9dbd5ce22e90bfa2e9a6f17732390c8b::Some 
 �    *  ]    __lock ��� )   __poison �   ^           std::sys_common::remutex::ReentrantMutexGuard<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>> 4c14be6d6b5137e63e0620e4c3497d6e �   �              std::sys_common::remutex::ReentrantMutexGuard<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>> 4c14be6d6b5137e63e0620e4c3497d6e �   �              std::sys_common::poison::PoisonError<std::sys_common::remutex::ReentrantMutexGuard<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>> 22f5ab40112b72c1f1de52794d54c22f ��Z  <    RUST$ENUM$DISR ��� `   __0 �� <    RUST$ENUM$DISR ��� a   __0 ��V  b   core::result::Result<std::sys_common::remutex::ReentrantMutexGuard<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>, std::sys_common::poison::PoisonError<std::sys_common::remutex::ReentrantMutexGuard<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>>> d948dcf466ad3cba65ec26549096a95c ��  `    guard �   d           std::sys_common::poison::PoisonError<std::sys_common::remutex::ReentrantMutexGuard<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>> 22f5ab40112b72c1f1de52794d54c22f ��  �    inner N   f           std::io::stdio::Stderr 88643e5dbe9228789c81964468046cac ��J   �              core::result::Ok d96f4bb8a13bda931c032ec94499078e::Ok &  h    RUST$ENCODED$ENUM$0$Err ��   i   core::result::Result<core::cell::RefMut<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>, core::cell::BorrowMutError> d96f4bb8a13bda931c032ec94499078e ��z   �              core::cell::RefMut<std::io::stdio::Maybe<std::io::stdio::StderrRaw>> a2bf71b29588147a1c15db6ce6fe4a37   k    __0 ��J   l           core::result::Ok d96f4bb8a13bda931c032ec94499078e::Ok 
 �    &  n    value  P   borrow ���z   o           core::cell::RefMut<std::io::stdio::Maybe<std::io::stdio::StderrRaw>> a2bf71b29588147a1c15db6ce6fe4a37 "  #     secs � u    nanos V   q           std::time::duration::Duration 459eb513244f09113cd7e26f79bd63e9 ���V   �              std::time::duration::Duration 459eb513244f09113cd7e26f79bd63e9 ���J  s    RUST$ENUM$DISR ��� s    RUST$ENUM$DISR ��� s   __0 ��b   t   core::option::Option<std::time::duration::Duration> ccc3093d91db6c7443704c82bab6ea70 �J   t   core::option::Option<u64> a6f44d45164f082d1d6413aca94b4478 ���V   �           std::sys::windows::net::Socket 96ca4a9b9e3bfd2ef198b94dabf76f3c ���   �              core::cell::UnsafeCell<mut alloc::arc::Arc<std::sync::mutex::Mutex<std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>>>>*> 1f94780a1ff186d2f247a1237f411045   x    value �   y           core::cell::Cell<mut alloc::arc::Arc<std::sync::mutex::Mutex<std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>>>>*> 5884988de0cffe24c994aad03a31fff9 ��  R    value �   {           core::cell::UnsafeCell<mut alloc::arc::Arc<std::sync::mutex::Mutex<std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>>>>*> 1f94780a1ff186d2f247a1237f411045 �   �              core::cell::UnsafeCell<mut alloc::arc::Arc<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>>>*> 970653a040cbdba66397b9dce4ecd33b   }    value �   ~           core::cell::Cell<mut alloc::arc::Arc<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>>>*> afa5c823f8d1f79c48a6d79b3ed96136 ��  D    value �   �           core::cell::UnsafeCell<mut alloc::arc::Arc<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>>>*> 970653a040cbdba66397b9dce4ecd33b �   �              core::cell::UnsafeCell<mut alloc::arc::Arc<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>>*> 77c751cc63d7f8c6611aef56b3140196 ���  �    value �   �           core::cell::Cell<mut alloc::arc::Arc<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>>*> d2bede921551a2c77f979b46c226835a �  �    value �   �           core::cell::UnsafeCell<mut alloc::arc::Arc<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>>*> 77c751cc63d7f8c6611aef56b3140196 ��� 	   C                 	   G                 	   �                 	   �                 	   d                 	   �                 	   �                V   �              std::sys::windows::stdio::Stdout 747ca43e0957f0b47c509b5a3b73756f  	   �                �   �      core::option::Option<alloc::arc::Arc<std::sync::mutex::Mutex<std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>>>>> 4b7eef69e1db81d9e81e9a5cff61ca84 ��� 	   �                 	   }                 	   �                z   �      core::result::Result<std::sys::windows::stdio::Stdin, std::io::error::Error> 1fc912ef261dc612fbb035ee786fae5e  	   �                 	   a                 	   Q                N   �              std::io::stdio::Stdin 89eea673a2ab9daff7e29a802ab160b7 ��� 	   �                 	   U                 	   .                B  �      core::result::Result<std::sync::mutex::MutexGuard<std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>>>, std::sys_common::poison::PoisonError<std::sync::mutex::MutexGuard<std::io::buffered::BufReader<std::io::stdio::Maybe<std::io::stdio::StdinRaw>>>>> 563eea56864d46f054fba5ffc26605e8  	   �                �   �      core::option::Option<alloc::arc::Arc<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>>>> 7a35e223246898c13b118ae8958a026b ��� 	   �                 	   �                 	   �                 	   �                 	   �                N   �              std::io::stdio::Stdout 8cf2e64e7bf5ad51abb3d24bb391a9c1 �� 	   �                 	   >                �  �      core::result::Result<std::sys_common::remutex::ReentrantMutexGuard<core::cell::RefCell<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>>, std::sys_common::poison::PoisonError<std::sys_common::remutex::ReentrantMutexGuard<core::cell::RefCell<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>>>> 822e72f7a26a4d92bdc9bc450530ca9b  	   �                 	   P                �   �      core::result::Result<core::cell::RefMut<std::io::buffered::LineWriter<std::io::stdio::Maybe<std::io::stdio::StdoutRaw>>>, core::cell::BorrowMutError> b4ab634dd5f3700238106669c5093f7a ��� 	   �                ^   �      core::result::Result<(), std::io::error::Error> 92cb5263dae4acea8423ca61a72fe920 � 	   �                z   �      core::option::Option<mut std::io::stdio::Maybe<std::io::stdio::StdoutRaw>*> 70238f244273de37ccda9a21f1085137 � 	   �                �   �      core::option::Option<alloc::arc::Arc<std::sys_common::remutex::ReentrantMutex<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>>> 9dbd5ce22e90bfa2e9a6f17732390c8b �� 	   �                 	   �                N   �              std::io::stdio::Stderr 88643e5dbe9228789c81964468046cac �� 	   �                 	   `                V  �      core::result::Result<std::sys_common::remutex::ReentrantMutexGuard<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>, std::sys_common::poison::PoisonError<std::sys_common::remutex::ReentrantMutexGuard<core::cell::RefCell<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>>>> d948dcf466ad3cba65ec26549096a95c �� 	   �                 	   �                �   �      core::result::Result<core::cell::RefMut<std::io::stdio::Maybe<std::io::stdio::StderrRaw>>, core::cell::BorrowMutError> d96f4bb8a13bda931c032ec94499078e �� 	   �                 	   �                �   �              std::thread::local::LocalKey<core::cell::RefCell<core::option::Option<alloc::boxed::Box<Write>>>> 95c929d552f4995d801c7625967bd4c1 ��� 	   �                 	   s                b   �      core::option::Option<std::time::duration::Duration> ccc3093d91db6c7443704c82bab6ea70 � 	   �                 	   B                 	   -                V   �              std::sys::windows::net::Socket 96ca4a9b9e3bfd2ef198b94dabf76f3c �� 	   �                �   �              core::cell::UnsafeCell<core::option::Option<core::cell::Cell<(u64, u64)>>> e4d0de8cca2108b6ab8a62f042bc0ce2 ��F  �    inner  �   dtor_registered �� �   dtor_running �r   �            std::thread::local::fast::Key<core::cell::Cell<(u64, u64)>> 44cff84872135f24284dc04df15a2bb8 �   �              core::ptr::Shared<alloc::arc::ArcInner<std::sync::mpsc::blocking::Inner>> c36dba17ea9f8529c21f2aff418cae14 ���r   �              core::marker::PhantomData<std::sync::mpsc::blocking::Inner> 4e45ae5ad59fc0d2866cd7c5f01a8047 �&  �    ptr �� �    phantom ��f   �           alloc::arc::Arc<std::sync::mpsc::blocking::Inner> e48e7c99771a2d7d4be44823bb4457c   X    pointer ��   �           core::ptr::Shared<alloc::arc::ArcInner<std::sync::mpsc::blocking::Inner>> c36dba17ea9f8529c21f2aff418cae14 ���r                core::marker::PhantomData<std::sync::mpsc::blocking::Inner> 4e45ae5ad59fc0d2866cd7c5f01a8047 �r   �              core::ptr::Shared<alloc::arc::ArcInner<std::path::PathBuf>> 19b620911970d3928f72b8cd45484c0e �f   �              core::marker::PhantomData<std::path::PathBuf> 860af8488937dfd9a1bd9b306f47a8ee ���&  �    ptr �� �    phantom ��Z   �           alloc::arc::Arc<std::path::PathBuf> 5f8392969ddfd748c414eb8828211548 �  A    pointer ��r   �           core::ptr::Shared<alloc::arc::ArcInner<std::path::PathBuf>> 19b620911970d3928f72b8cd45484c0e �f                core::marker::PhantomData<std::path::PathBuf> 860af8488937dfd9a1bd9b306f47a8ee ���N   �              std::net::ip::Ipv6Addr ef5c1ae55ead2c174cd960c0b58e2d8f ��J  �    RUST$ENUM$DISR ��� �    RUST$ENUM$DISR ��� �   __0 ��Z   �   core::option::Option<std::net::ip::Ipv6Addr> b7b1abfba3ce8b711c7f0e2588ec9de0   �    inner N   �           std::net::ip::Ipv6Addr ef5c1ae55ead2c174cd960c0b58e2d8f ��&    u   r  core::option::Option ���J  �    RUST$ENUM$DISR ��� �    RUST$ENUM$DISR ��� �   __0 ��b   �   core::option::Option<std::net::addr::SocketAddrV4> 7daedb78658fd6b82cbdb8cb222ecfbe ��J  �    RUST$ENUM$DISR ��� �    RUST$ENUM$DISR ��� �   __0 ��b   �    core::option::Option<std::net::addr::SocketAddrV6> 238c01db316412d5847de4a597fc78dd ��N   �              std::net::ip::Ipv4Addr 3c7915eb1308b5513a7346b3d5cb7c21 ��J  �    RUST$ENUM$DISR ��� �    RUST$ENUM$DISR ��� �   __0 ��Z   �   core::option::Option<std::net::ip::Ipv4Addr> dfcae15b24f64f317bbdb10190f699d4   �    inner N   �           std::net::ip::Ipv4Addr 3c7915eb1308b5513a7346b3d5cb7c21 ��V   �              std::sys_common::wtf8::CodePoint a8076ce458080eb4224f04087497faf �J  �    RUST$ENUM$DISR ��� �    RUST$ENUM$DISR ��� �   __0 ��f   �   core::option::Option<std::sys_common::wtf8::CodePoint> 6008d4c7e1a591d381587a8f0e9d70fc ��  u     value V   �           std::sys_common::wtf8::CodePoint a8076ce458080eb4224f04087497faf �n   �              core::hash::sip::Hasher<core::hash::sip::Sip13Rounds> b5cbbb927b0dadafd8004b7d439c8c34 ���  �    hasher ���R   �          H core::hash::sip::SipHasher13 17884aaa2d9e621f9aadb95196e54b84 N   �              core::hash::sip::State ac927a2aa0822e5fcc04bdb209f6e4fe ��n   �              core::marker::PhantomData<core::hash::sip::Sip13Rounds> 3ebcb6994a484006f7d86e085017f568 �z  #     k0 ��� #    k1 ��� #    length ��� �   state  #   8 tail � #   @ ntail  �    _marker ��n   �          H core::hash::sip::Hasher<core::hash::sip::Sip13Rounds> b5cbbb927b0dadafd8004b7d439c8c34 ���B  #     v0 ��� #    v2 ��� #    v1 ��� #    v3 ���N   �            core::hash::sip::State ac927a2aa0822e5fcc04bdb209f6e4fe ��n                core::marker::PhantomData<core::hash::sip::Sip13Rounds> 3ebcb6994a484006f7d86e085017f568 �R   �              core::hash::sip::SipHasher13 17884aaa2d9e621f9aadb95196e54b84   �    __0 ��b   �          H std::collections::hash::map::DefaultHasher 6511b9a8d7638b52b7c4a824068072f ���B   �              slice<u8>* 3d56b4bd09967ef33c47b297d51dadd ���  �    s  #    pos ��N   �           std::net::parser::Parser 672fc8cd2c84141ccaaec9cc7e393f51 B   H           slice<u8>* 3d56b4bd09967ef33c47b297d51dadd ���J   �              core::result::Ok b4df4a30b68f27be65744431707fed31::Ok &  �    RUST$ENCODED$ENUM$0$Err ��   �    core::result::Result<std::net::addr::SocketAddr, std::net::parser::AddrParseError> b4df4a30b68f27be65744431707fed31 ��  �    __0 ��J   �            core::result::Ok b4df4a30b68f27be65744431707fed31::Ok V   �              core::str::SplitInternal<char> 3368a6afa0a0266cfec3826aa85bf19b ��"       iter � #   H count V             P core::str::SplitNInternal<char> 4091c7a31983c544c80e92395e0bb9a8 �V   �              core::str::pattern::CharSearcher b59922ebd620ca33a2659dba53571d8b j  #     start  #    end ��    matcher �� 0   @ allow_trailing_empty � 0   A finished �V             H core::str::SplitInternal<char> 3368a6afa0a0266cfec3826aa85bf19b ��     #     �  C    haystack � #    finger ��� #    finger_back ��     ( needle ��� #     utf8_size    , utf8_encoded �V             0 core::str::pattern::CharSearcher b59922ebd620ca33a2659dba53571d8b V   �              std::sys_common::net::TcpStream d42ea69474431dc3887754ce3ae4a621 �Z  <    RUST$ENUM$DISR ��� 	   __0 �� <    RUST$ENUM$DISR ��� �   __0 ��z   
   core::result::Result<std::sys_common::net::TcpStream, std::io::error::Error> bb7eb3ef1f913e9611975ff78d05508b   �    inner V              std::sys_common::net::TcpStream d42ea69474431dc3887754ce3ae4a621 �V              std::sys_common::net::TcpListener 97058cf5ef74606a8adceed96c59be0 V   �              std::sys_common::net::TcpListener 97058cf5ef74606a8adceed96c59be0 Z  <    RUST$ENUM$DISR ���    __0 �� <    RUST$ENUM$DISR ��� �   __0 ��~      core::result::Result<std::sys_common::net::TcpListener, std::io::error::Error> 70c05216b4d24e1e90a9a931e0f3c202 ��r   �              (std::sys_common::net::TcpStream, std::net::addr::SocketAddr) 13eefd1f7cbfd69955c1b03ce58dc2e Z  <    RUST$ENUM$DISR ���    __0 �� <    RUST$ENUM$DISR ��� �   __0 ��     0 core::result::Result<(std::sys_common::net::TcpStream, std::net::addr::SocketAddr), std::io::error::Error> fdc20b624f8d1cb020d528ea8a30d141 ��"  	    __0 �� �   __1 ��r             ( (std::sys_common::net::TcpStream, std::net::addr::SocketAddr) 13eefd1f7cbfd69955c1b03ce58dc2e n   �              (std::net::tcp::TcpStream, std::net::addr::SocketAddr) 82f8533e916ba519115de073e631b73 ���Z  <    RUST$ENUM$DISR ���    __0 �� <    RUST$ENUM$DISR ��� �   __0 ��     0 core::result::Result<(std::net::tcp::TcpStream, std::net::addr::SocketAddr), std::io::error::Error> 4f5b9d6e173d4fde5ac0f93c7750d34a �N   �              std::net::tcp::TcpStream c8968fb22c1ca548465a757795b3429 �"      __0 �� �   __1 ��n             ( (std::net::tcp::TcpStream, std::net::addr::SocketAddr) 82f8533e916ba519115de073e631b73 ���  	    __0 ��N              std::net::tcp::TcpStream c8968fb22c1ca548465a757795b3429 �N   �              core::option::Some 4c172857756d821117f943e176350dc0::Some &      RUST$ENCODED$ENUM$0$None �Z       core::option::Option<std::net::ip::IpAddr> 4c172857756d821117f943e176350dc0 ��B   �      std::net::ip::IpAddr 2a2809a26b802890eb86e6dc7aa4bb7b   "    __0 ��N   #           core::option::Some 4c172857756d821117f943e176350dc0::Some &        �  std::net::ip::IpAddr ���Z  %    RUST$ENUM$DISR ��� �   __0 �� %    RUST$ENUM$DISR ��� �   __0 ��B   &   std::net::ip::IpAddr 2a2809a26b802890eb86e6dc7aa4bb7b N   �              core::option::Some 3cafe5b8cbfcb06b75fe574a99aa2b0::Some �&  (    RUST$ENCODED$ENUM$0$None �J   )   core::option::Option<char> 3cafe5b8cbfcb06b75fe574a99aa2b0 ���        __0 ��N   +           core::option::Some 3cafe5b8cbfcb06b75fe574a99aa2b0::Some �J  �    RUST$ENUM$DISR ��� �    RUST$ENUM$DISR ���      __0 ��F   -   core::option::Option<u8> ff4de8d89f1dbf31e15d6947f587e662 J  �    RUST$ENUM$DISR ��� �    RUST$ENUM$DISR ��� u    __0 ��J   /   core::option::Option<u32> a64a28f6af3c043543232617e4f12690 ���&    !   r  core::option::Option ���J  1    RUST$ENUM$DISR ��� 1    RUST$ENUM$DISR ��� !    __0 ��J   2   core::option::Option<u16> cee56cc74218baa6d67fc90967ed06ff ���N   �              core::option::Some 61da7cca55f9456131b718d8762e9987::Some &  4    RUST$ENCODED$ENUM$0$None �j   5   core::option::Option<(std::net::ip::Ipv4Addr, char, u16)> 61da7cca55f9456131b718d8762e9987 ���Z   �              (std::net::ip::Ipv4Addr, char, u16) cc19f7a21877b4136f12b582f6feb4da �  7    __0 ��N   8           core::option::Some 61da7cca55f9456131b718d8762e9987::Some 2  �    __0 ��      __1 �� !    __2 ��Z   :           (std::net::ip::Ipv4Addr, char, u16) cc19f7a21877b4136f12b582f6feb4da �N   �              core::option::Some 56a4c5fdfb61ed9d1ab2581179fea20::Some �&  <    RUST$ENCODED$ENUM$0$None �f   =   core::option::Option<(std::net::ip::Ipv6Addr, char, u16)> 56a4c5fdfb61ed9d1ab2581179fea20 Z   �              (std::net::ip::Ipv6Addr, char, u16) 1c3b087a38655aaee42038ef5d88d525 �  ?    __0 ��N   @           core::option::Some 56a4c5fdfb61ed9d1ab2581179fea20::Some �2  �   __0 ��       __1 �� !    __2 ��Z   B           (std::net::ip::Ipv6Addr, char, u16) 1c3b087a38655aaee42038ef5d88d525 �N   �              core::option::Some d5be91220e4563c46e27c2d3454336d1::Some &  D    RUST$ENCODED$ENUM$0$None �^   E    core::option::Option<std::net::addr::SocketAddr> d5be91220e4563c46e27c2d3454336d1 N   �            core::option::Some d5be91220e4563c46e27c2d3454336d1::Some N   �              core::option::Some c5d85e8940d7a157f6045c41b2de4464::Some &  H    RUST$ENCODED$ENUM$0$None �b   I   core::option::Option<mut core::char::EscapeDebug*> c5d85e8940d7a157f6045c41b2de4464 ��N   �              core::char::EscapeDebug 63d55f5be4ca5b52789624843b0c77d6 �
 K      L    __0 ��N   M           core::option::Some c5d85e8940d7a157f6045c41b2de4464::Some R   �              core::char::EscapeDefault 90e7d3d3f0423f6fad9e93bc4becf7e2 ���  O    __0 ��N   P           core::char::EscapeDebug 63d55f5be4ca5b52789624843b0c77d6 �N   �      core::char::EscapeDefaultState a28de1f1da766abb6e61773331c52529 ��  R    state R   S           core::char::EscapeDefault 90e7d3d3f0423f6fad9e93bc4becf7e2 ���:    Done �  Char �  Backslash   Unicode ��.    u   U  core::char::EscapeDefaultState �R   �              core::char::EscapeUnicode 12cc514a6e19fa07958f8bbc4864e297 ���  V    RUST$ENUM$DISR ��� V    RUST$ENUM$DISR ���      __0 �� V    RUST$ENUM$DISR ���      __0 �� V    RUST$ENUM$DISR ��� W   __0 ��N   X   core::char::EscapeDefaultState a28de1f1da766abb6e61773331c52529 ��Z    Done �  RightBrace ���  Value   LeftBrace   Type �  Backslash .        Z  core::char::EscapeUnicodeState �6       c  [   state  #     hex_digit_idx R   \           core::char::EscapeUnicode 12cc514a6e19fa07958f8bbc4864e297 ���f                std::collections::hash::map::DefaultResizePolicy 1d2a43683d382404036599bd5f08102 �      __0 ��R   _           std::net::tcp::TcpListener fac5a393589778d9384439575c773d6c ��b   �      core::option::Option<core::cell::Cell<(u64, u64)>> 112ee47442de9ebd45c8f8d956e22f0d ��  a    value �   b           core::cell::UnsafeCell<core::option::Option<core::cell::Cell<(u64, u64)>>> e4d0de8cca2108b6ab8a62f042bc0ce2 ��R   �              core::cell::Cell<(u64, u64)> d3da6cf3818327bb44e2e18d6dc4bd1d J  s    RUST$ENUM$DISR ��� s    RUST$ENUM$DISR ��� d   __0 ��b   e   core::option::Option<core::cell::Cell<(u64, u64)>> 112ee47442de9ebd45c8f8d956e22f0d ��Z   �              core::cell::UnsafeCell<(u64, u64)> ea5cf2fe3e76197aef73c8b6d5fd8e20 ��  g    value R   h           core::cell::Cell<(u64, u64)> d3da6cf3818327bb44e2e18d6dc4bd1d B   �              (u64, u64) 9468d05bedab70a1a85848b21e6148cf ��  j    value Z   k           core::cell::UnsafeCell<(u64, u64)> ea5cf2fe3e76197aef73c8b6d5fd8e20 ��"  #     __0 �� #    __1 ��B   m           (u64, u64) 9468d05bedab70a1a85848b21e6148cf ��f   �              alloc::arc::Arc<std::sync::mpsc::blocking::Inner> e48e7c99771a2d7d4be44823bb4457c  	   o                 	   �                 	   i                Z   �              alloc::arc::Arc<std::path::PathBuf> 5f8392969ddfd748c414eb8828211548 � 	   s                 	   �                 	   �                 	   �                Z   �      core::option::Option<std::net::ip::Ipv6Addr> b7b1abfba3ce8b711c7f0e2588ec9de0  	   x                b   �      core::option::Option<std::net::addr::SocketAddrV4> 7daedb78658fd6b82cbdb8cb222ecfbe �� 	   z                b   �      core::option::Option<std::net::addr::SocketAddrV6> 238c01db316412d5847de4a597fc78dd �� 	   |                Z   �      core::option::Option<std::net::ip::Ipv4Addr> dfcae15b24f64f317bbdb10190f699d4  	   ~                 	   �                 	                    f   �      core::option::Option<std::sys_common::wtf8::CodePoint> 6008d4c7e1a591d381587a8f0e9d70fc �� 	   �                f   �              std::collections::hash::map::DefaultResizePolicy 1d2a43683d382404036599bd5f08102 � 	   �                b   �              std::collections::hash::map::DefaultHasher 6511b9a8d7638b52b7c4a824068072f ��� 	   �                 	   �                 	   �                N   �              std::net::parser::Parser 672fc8cd2c84141ccaaec9cc7e393f51  	   �                ~   �      core::result::Result<std::sys_common::net::LookupHost, std::io::error::Error> a7d1afa9832edadf8165ff59e5337985 ��� 	   �                �   �      core::result::Result<std::net::addr::SocketAddr, std::net::parser::AddrParseError> b4df4a30b68f27be65744431707fed31 �� 	   �                V   �              core::str::SplitNInternal<char> 4091c7a31983c544c80e92395e0bb9a8 � 	   �                 	                   z   �      core::result::Result<std::sys_common::net::TcpStream, std::io::error::Error> bb7eb3ef1f913e9611975ff78d05508b  	   �                 	   	                z   �      core::result::Result<std::sys::windows::net::Socket, std::io::error::Error> 34f21cd9d02a45b0f7f6f6099a850bd9 � 	   �                R   �              std::net::tcp::TcpListener fac5a393589778d9384439575c773d6c �� 	   �                 	                   ~   �      core::result::Result<std::sys_common::net::TcpListener, std::io::error::Error> 70c05216b4d24e1e90a9a931e0f3c202 �� 	   �                �   �      core::result::Result<(std::sys_common::net::TcpStream, std::net::addr::SocketAddr), std::io::error::Error> fdc20b624f8d1cb020d528ea8a30d141 �� 	   �                �   �      core::result::Result<(std::net::tcp::TcpStream, std::net::addr::SocketAddr), std::io::error::Error> 4f5b9d6e173d4fde5ac0f93c7750d34a � 	   �                Z   �      core::option::Option<std::net::ip::IpAddr> 4c172857756d821117f943e176350dc0 �� 	   �                J   �      core::option::Option<char> 3cafe5b8cbfcb06b75fe574a99aa2b0 ��� 	   �                 	   �                F   �      core::option::Option<u8> ff4de8d89f1dbf31e15d6947f587e662  	   �                J   �      core::option::Option<u32> a64a28f6af3c043543232617e4f12690 ��� 	   �                J   �      core::option::Option<u16> cee56cc74218baa6d67fc90967ed06ff ��� 	   �                 	   �                j   �      core::option::Option<(std::net::ip::Ipv4Addr, char, u16)> 61da7cca55f9456131b718d8762e9987 ��� 	   �                f   �      core::option::Option<(std::net::ip::Ipv6Addr, char, u16)> 56a4c5fdfb61ed9d1ab2581179fea20  	   �                ^   �      core::option::Option<std::net::addr::SocketAddr> d5be91220e4563c46e27c2d3454336d1  	   �                J   �      core::option::Option<u16*> 777f67ff840f8f8d39e9109669e7c1b5 �� 	   �                b   �      core::option::Option<mut core::char::EscapeDebug*> c5d85e8940d7a157f6045c41b2de4464 �� 	   �                R   �              alloc::string::FromUtf8Error 90c2ee0de53498bff9d3ff32e14d9e7c Z  <    RUST$ENUM$DISR ��� P   __0 �� <    RUST$ENUM$DISR ��� �   __0 ��z   �  0 core::result::Result<alloc::string::String, alloc::string::FromUtf8Error> 3ab796cb644cc1eb26f43af4955bc794 ���J   �              core::str::Utf8Error 825164c853f026ddd9b634028739e8e8 "  5    bytes  �   error R   �          ( alloc::string::FromUtf8Error 90c2ee0de53498bff9d3ff32e14d9e7c .  #     valid_up_to �� �   error_len J   �           core::str::Utf8Error 825164c853f026ddd9b634028739e8e8 N   �              core::option::Some 273a8dfd161ac03e7468024dd0aa766::Some �&  �    RUST$ENCODED$ENUM$0$None �F   �   core::option::Option<u8*> 273a8dfd161ac03e7468024dd0aa766 N   )           core::option::Some 273a8dfd161ac03e7468024dd0aa766::Some �Z  <    RUST$ENUM$DISR ��� P   __0 �� <    RUST$ENUM$DISR ��� 2   __0 ��z   �    core::result::Result<alloc::string::String, std::sys_common::wtf8::Wtf8Buf> 9571f2bc429e6b35289a1699d85beba2 �"  l    inner  �   error V   �            std::ffi::c_str::IntoStringError e85abb3fa615c679d4fd9b3e22eb7814 z   �      core::result::Result<alloc::string::String, alloc::string::FromUtf8Error> 3ab796cb644cc1eb26f43af4955bc794 ��� 	   �                F   �      core::option::Option<u8*> 273a8dfd161ac03e7468024dd0aa766  	   �                 	   m                V   �              std::ffi::c_str::IntoStringError e85abb3fa615c679d4fd9b3e22eb7814  	   �                 	   �                 	   "                z   �      core::result::Result<alloc::string::String, std::sys_common::wtf8::Wtf8Buf> 9571f2bc429e6b35289a1699d85beba2 � 	   �                f   �              core::ptr::Unique<std::ffi::os_str::OsString> c247a8b87b27f7e06c9a3145386cf271 ���.  �    ptr �� #    cap ��     a ~   �           alloc::raw_vec::RawVec<std::ffi::os_str::OsString, alloc::heap::Heap> fd1c5a58533c1ed089015047d41edb78 ���j   �              core::marker::PhantomData<std::ffi::os_str::OsString> 50ed573089d62b02999b73e79ac228 �*  7    pointer �� �    _marker ��f   �           core::ptr::Unique<std::ffi::os_str::OsString> c247a8b87b27f7e06c9a3145386cf271 ���j                core::marker::PhantomData<std::ffi::os_str::OsString> 50ed573089d62b02999b73e79ac228 �f   �              core::ptr::Unique<std::net::addr::SocketAddr> c50dbb56f4b6c2b549c8ab35a97fc78e ���.  �    ptr �� #    cap ��     a ~   �           alloc::raw_vec::RawVec<std::net::addr::SocketAddr, alloc::heap::Heap> 61ff9f2e0e5ace71198e93bca25c1be7 ���n   �              core::marker::PhantomData<std::net::addr::SocketAddr> 37bfd390dcb81cfd6131abe81c68c6b5 ���*  S    pointer �� �    _marker ��f   �           core::ptr::Unique<std::net::addr::SocketAddr> c50dbb56f4b6c2b549c8ab35a97fc78e ���n                core::marker::PhantomData<std::net::addr::SocketAddr> 37bfd390dcb81cfd6131abe81c68c6b5 ���   �      core::option::Option<core::cell::UnsafeCell<core::option::Option<core::cell::RefCell<core::option::Option<alloc::boxed::Box<Write>>>>>*> 884ca6268fdd7bbf609cb10399bbc6bc  �         
 �     �         
 �    "  �    inner  �   init �   �           std::thread::local::LocalKey<core::cell::RefCell<core::option::Option<alloc::boxed::Box<Write>>>> 95c929d552f4995d801c7625967bd4c1 ���N   �              core::option::Some 884ca6268fdd7bbf609cb10399bbc6bc::Some &  �    RUST$ENCODED$ENUM$0$None �   �   core::option::Option<core::cell::UnsafeCell<core::option::Option<core::cell::RefCell<core::option::Option<alloc::boxed::Box<Write>>>>>*> 884ca6268fdd7bbf609cb10399bbc6bc 
 �     