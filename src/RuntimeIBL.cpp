#include "RuntimeIBL.h"
#include "runtime_ibl_c_api.h"
#include "rgbe.h"
namespace AlexQPBR {
    float GetIlumination(float R, float G, float B)
    {
        return 0.3*R + 0.59*G + 0.11*B;
    }
    CubeMap::CubeMap(int width, int height, int mips)
        : mWidth(width), mHeight(height)
        , mMipLevels(mips)
        , mGLHandle(0)
        , mHDR(false)
        , mCylinder(false)
        , mFloatData(nullptr)
    {
        GeneratePyramids();
    }
    CubeMap::CubeMap(const char* path, bool inCylinder)
        : mWidth(0), mHeight(0)
        , mMipLevels(1)
        , mGLHandle(0)
        , mHDR(true)
        , mCylinder(inCylinder)
        , mFloatData(nullptr)
    {
        FILE* hdr_file = fopen(path, "rb");
        rgbe_header_info header = {};
        auto result = RGBE_ReadHeader(hdr_file, &mWidth, &mHeight, &header);
        if (result == RGBE_RETURN_FAILURE) {
            if (hdr_file)
                fclose(hdr_file);
            return;
        }
        mFloatData = new float[3 * mWidth * mHeight];
        result = RGBE_ReadPixels_RLE(hdr_file, mFloatData, mWidth, mHeight);
        size_t pixels = mWidth * mHeight;
        float maxIllum = 0.0f;
        for (size_t i = 0; i < pixels; i++)
        {
            float illum = GetIlumination(mFloatData[3 * i], mFloatData[3 * i + 1], mFloatData[3 * i + 2]);
            if (illum > maxIllum)
            {
                maxIllum = illum;
            }
        }
        mMaxIllum = maxIllum;
        fclose(hdr_file);
        CreateGLCubemap();
    }
    CubeMap* CubeMap::LoadHDR(const char* hdr_path)
    {
        return new CubeMap(hdr_path, true);
    }
    float CubeMap::MaxIllumi() const
    {
        return mMaxIllum;
    }
    bool CubeMap::IsHDR() const
    {
        return mHDR;
    }
    void CubeMap::GeneratePyramids()
    {
        for (auto& face : mFaces)
        {
            face.resize(mMipLevels);
            for (int i = 0; i < mMipLevels; i++)
            {
                auto& mipTex = face[i];
                mipTex.resize((mWidth >> i) * (mHeight >> i));
            }
        }
    }
    CubeMap::~CubeMap()
    {
        if (glIsTexture(mGLHandle))
        {
            glDeleteTextures(1, &mGLHandle);
            mGLHandle = 0;
        }
        if (mHDR)
        {
            if (mFloatData)
            {
                delete[]mFloatData;
            }
        }
    }
	bool CubeMap::UseRGBM() const
	{
		return mUseRGBM;
	}
	void CubeMap::SetEncodeRGBM(bool bRGBM, float maxRgbm)
	{
		mUseRGBM = bRGBM;
		mMaxRGBM = maxRgbm;
	}
    EPixelFormat CubeMap::GetPixelFormat() const
    {
        return EPixelFormat::EPF_ARGB32;
    }
    int CubeMap::GetWidth() const
    {
        return mWidth;
    }
    int CubeMap::GetHeight() const
    {
        return mHeight;
    }
    int CubeMap::GetLevels() const
    {
        return mMipLevels;
    }
    const ColorARGB * CubeMap::GetPixels(EFace face, int mip) const
    {
        return mFaces[face][mip].data();
    }
    ColorARGB * CubeMap::GetPixels(EFace face, int mip)
    {
        return mFaces[face][mip].data();
    }
    void CubeMap::SetPixels(EFace face, int mip, ColorARGB* pixels)
    {
        size_t Count = *((size_t*)pixels - 1);
        if (Count == mFaces[face][mip].size())
        {
            ColorARGB* data = mFaces[face][mip].data();
            memcpy(data, pixels, Count * sizeof(ColorARGB));
        }
        else
        {
            printf("Failed to SetPixels.\n");
        }
    }
    GLuint CubeMap::CreateGLCubemap()
    {
        assert(glGetError() == GL_NO_ERROR);
        glGenTextures(1, &mGLHandle);
        if (mHDR && mCylinder)
        {
            glBindTexture(GL_TEXTURE_2D, mGLHandle);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, mWidth, mHeight, 0, GL_RGB, GL_FLOAT, mFloatData);
            glBindTexture(GL_TEXTURE_2D, 0);
        }
        else
        {
            glBindTexture(GL_TEXTURE_CUBE_MAP, mGLHandle);
            int mipLevel = 0;
            for (int index = 0; index < 6; index++) {
                //FlipImage(&pCubeMap->faces[index]);
                glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + index, mipLevel, GL_RGBA, mWidth, mWidth, 0, GL_RGBA, GL_FLOAT, mFaces[index][mipLevel].data());
                //FlipImage(&pCubeMap->faces[index]);
            }
            glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
        }

        assert(glGetError() == GL_NO_ERROR);
        return mGLHandle;
    }
    void CubeMap::Bind()
    {
        if (mCylinder)
        {
            glBindTexture(GL_TEXTURE_2D, GetGLHandle());
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        }
        else
        {
            glBindTexture(GL_TEXTURE_CUBE_MAP, GetGLHandle());
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        }
    }
}
#define RUNTIMEIBL_API extern "C" __declspec(dllexport) 

RUNTIMEIBL_API
void UnityPluginLoad()
{
    glewInit();
}
// Unity plugin unload event
RUNTIMEIBL_API
void UnityPluginUnload()
{

}

ibl_cubemap_t ibl_hdr_load(const char* hdr_path)
{
    return (ibl_cubemap_t)AlexQPBR::CubeMap::LoadHDR(hdr_path);
}

ibl_cubemap_t ibl_create_cubemap(int w, int h, int mips) {
    return (ibl_cubemap_t)new AlexQPBR::CubeMap(w, h, mips);
}

void ibl_destroy_cubemap(ibl_cubemap_t c)
{
    delete (AlexQPBR::CubeMap*)(c);
}

bool ibl_cubemap_is_hdr(ibl_cubemap_t c)
{
    return ((AlexQPBR::CubeMap*)c)->IsHDR();
}

int ibl_cubemap_get_width(ibl_cubemap_t c)
{
    return ((AlexQPBR::CubeMap*)c)->GetWidth();
}

int ibl_cubemap_get_height(ibl_cubemap_t c)
{
    return ((AlexQPBR::CubeMap*)c)->GetHeight();
}

int ibl_cubemap_get_mips(ibl_cubemap_t c)
{
    return ((AlexQPBR::CubeMap*)c)->GetLevels();
}

float ibl_cubemap_get_max_illum(ibl_cubemap_t c)
{
    return ((AlexQPBR::CubeMap*)c)->MaxIllumi();
}

bool ibl_cubemap_is_rgbm_encoded(ibl_cubemap_t c)
{
	return ((AlexQPBR::CubeMap*)c)->UseRGBM();
}

void ibl_cubemap_save_dds(ibl_cubemap_t c, const char * path)
{
    ((AlexQPBR::CubeMap*)c)->SaveDDS(path);
}

void ibl_cubemap_filter_and_generate_mips(ibl_cubemap_t src, ibl_cubemap_t dest, int samples, bool useRGBM, float maxRGBM)
{
	AlexQPBR::IBLRenderer renderer(useRGBM, maxRGBM);
	renderer.GenerateFilteredMipmapChain((AlexQPBR::CubeMap*)src, (AlexQPBR::CubeMap*)dest, samples);
}

int ibl_cubemap_create_gl_handle(ibl_cubemap_t c)
{
    return ((AlexQPBR::CubeMap*)c)->CreateGLCubemap();
}

int ibl_cubemap_get_gl_handle(ibl_cubemap_t c)
{
    return ((AlexQPBR::CubeMap*)c)->GetGLHandle();
}

void ibl_cubemap_set_pixels(ibl_cubemap_t c, enum ibl_cubemap_face face, int mip, const ibl_color_rgba* pixels)
{
    ((AlexQPBR::CubeMap*)c)->SetPixels((AlexQPBR::CubeMap::EFace)face, mip, (AlexQPBR::ColorARGB*)pixels);
}

ibl_color_rgba * ibl_cubemap_get_pixels(ibl_cubemap_t c, ibl_cubemap_face face, int mip)
{
    return (ibl_color_rgba*)((AlexQPBR::CubeMap*)c)->GetPixels((AlexQPBR::CubeMap::EFace)face, mip);
}
