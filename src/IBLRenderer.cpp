#include "RuntimeIBL.h"

#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/quaternion.hpp"
#include "glm/gtc/constants.hpp"

namespace AlexQPBR
{
    struct vertex {
        float position[3];
        float texcoord[2];
    };
    static const vertex vertices[4] = {
    { { -1.0f, -1.0f, 0.0f },{ -1.0f, -1.0f } },
    { { 1.0f, -1.0f, 0.0f },{ 1.0f, -1.0f } },
    { { 1.0f,  1.0f, 0.0f },{ 1.0f,  1.0f } },
    { { -1.0f,  1.0f, 0.0f },{ -1.0f,  1.0f } },
    };
    static const unsigned short indices[6] = { 0, 1, 2, 2, 3, 0 };

    RenderTexture::RenderTexture(int width, int height)
        : mWidth(width), mHeight(height), mFBO(0), mRBO(0)
    {
        glGenTextures(1, &mTex);
        glBindTexture(GL_TEXTURE_2D, mTex);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mWidth, mHeight, 0, GL_RGBA, GL_FLOAT, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glBindTexture(GL_TEXTURE_2D, 0);

        glGenRenderbuffers(1, &mRBO);
        glBindRenderbuffer(GL_RENDERBUFFER, mRBO);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, mWidth, mHeight);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);

        glGenFramebuffers(1, &mFBO);
        glBindFramebuffer(GL_FRAMEBUFFER, mFBO);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mTex, 0);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, mRBO);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE;
    }

    RenderTexture::~RenderTexture()
    {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glDeleteFramebuffers(1, &mFBO);
        glDeleteRenderbuffers(1, &mRBO);
        glDeleteTextures(1, &mTex);
    }

    void RenderTexture::Bind()
    {
        glBindFramebuffer(GL_FRAMEBUFFER, mFBO);
        glViewport(0, 0, mWidth, mHeight);
        glScissor(0, 0, mWidth, mHeight);
        glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
        glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_ALPHA_TEST);
        glDisable(GL_BLEND);
        glDisable(GL_CULL_FACE);
    }

    void RenderTexture::Unbind()
    {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    QuadRender::QuadRender()
    {
        InitBuffer();
    }
    void QuadRender::InitBuffer()
    {
        glGenBuffers(1, &mVBO);
        glBindBuffer(GL_ARRAY_BUFFER, mVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertex) * 4, vertices, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glGenBuffers(1, &mIBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short) * 6, indices, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
    QuadRender::~QuadRender()
    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glDeleteBuffers(1, &mVBO);
        glDeleteBuffers(1, &mIBO);
    }
    void QuadRender::Bind()
    {
        glBindBuffer(GL_ARRAY_BUFFER, mVBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIBO);
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (GLvoid *)0); // position
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), (GLvoid *)12);// texcoord
    }
    void QuadRender::Unbind()
    {
        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
    void QuadRender::Draw()
    {
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, NULL);
    }
    IBLRenderer::IBLRenderer(bool useRGBM, float maxRGBM) : mUseRGBMEncode(useRGBM), mMaxRGBM(maxRGBM)
    {

    }
    IBLRenderer::~IBLRenderer()
    {
    }
    void IBLRenderer::GenerateFilteredMipmapChain(CubeMap* src, CubeMap* dest, int samples)
    {
        assert(glGetError() == GL_NO_ERROR);
        int mips = dest->GetLevels();
        GpuConvolveProgram progConvolve(mUseRGBMEncode);
        assert(glGetError() == GL_NO_ERROR);
        QuadRender quadRender;
        assert( glGetError() == GL_NO_ERROR );
        for (int mipLevel = 0; mipLevel < mips; mipLevel++)
        {
            int mipWidth = dest->GetWidth() >> mipLevel;
            int mipHeight = dest->GetHeight() >> mipLevel;
            RenderTexture Rt(mipWidth, mipHeight);
            Rt.Bind();
            progConvolve.Bind();
            //glEnable(GL_TEXTURE_CUBE_MAP);
            glActiveTexture(GL_TEXTURE0);
            src->Bind();
            {
                glm::mat4 matModeView = glm::lookAt(glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
                glm::mat4 matProjection = glm::ortho(-1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f);
                glm::mat4 matModeViewProjection = matProjection * matModeView;
                glm::mat4 matTexcoords[6] = {
                    glm::rotate(glm::mat4(),  glm::pi<float>() / 2.0f, glm::vec3(0.0f, 1.0f, 0.0f)),
                    glm::rotate(glm::mat4(), -glm::pi<float>() / 2.0f, glm::vec3(0.0f, 1.0f, 0.0f)),
                    glm::rotate(glm::mat4(), -glm::pi<float>() / 2.0f, glm::vec3(1.0f, 0.0f, 0.0f)),
                    glm::rotate(glm::mat4(),  glm::pi<float>() / 2.0f, glm::vec3(1.0f, 0.0f, 0.0f)),
                    glm::mat4(),
                    glm::rotate(glm::mat4(),  glm::pi<float>(), glm::vec3(0.0f, 1.0f, 0.0f)),
                };
                progConvolve.SetInt("_level", mipLevel);
                progConvolve.SetTexture("_sphericalMap", 0);
                progConvolve.SetInt("_samples", samples);
                progConvolve.SetMatrix("_modelViewProjectionMatrix", matModeViewProjection);

				if (progConvolve.UseRGBM()) {
					progConvolve.SetFloat("_maxRGBM", mMaxRGBM);
				}

                quadRender.Bind();
                for (int faceIndex = 0; faceIndex < 6; faceIndex++)
                {
                    assert(glGetError() == GL_NO_ERROR);
                    progConvolve.SetMatrix("_texcoordMatrix", matTexcoords[faceIndex]);
                    quadRender.Draw();
                    glReadPixels(0, 0, mipWidth, mipHeight, GL_RGBA, GL_FLOAT, dest->GetPixels((CubeMap::EFace)faceIndex, mipLevel));
                    assert(glGetError() == GL_NO_ERROR);
                }
                quadRender.Unbind();
                progConvolve.Unbind();
            }
            Rt.Unbind();
        }

    }
}