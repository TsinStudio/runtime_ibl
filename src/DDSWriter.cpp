#include "RuntimeIBL.h"
#include <stdint.h>
#include <stdio.h>
namespace AlexQPBR
{
#define DDSD_CAPS         0x00000001
#define DDSD_HEIGHT       0x00000002
#define DDSD_WIDTH        0x00000004
#define DDSD_PITCH        0x00000008
#define DDSD_PIXELFORMAT  0x00001000
#define DDSD_MIPMAPCOUNT  0x00020000
#define DDSD_LINEARSIZE   0x00080000
#define DDSD_DEPTH        0x00800000

#define DDPF_ALPHAPIXELS  0x00000001
#define DDPF_FOURCC       0x00000004
#define DDPF_RGB          0x00000040
#define DDPF_LUMINANCE    0x00020000

#define FOURCC_DXT1       0x31545844
#define FOURCC_DXT3       0x33545844
#define FOURCC_DXT5       0x35545844

#define DDSCAPS_COMPLEX   0x00000008
#define DDSCAPS_TEXTURE   0x00001000
#define DDSCAPS_MIPMAP    0x00400000

#define DDS_CUBEMAP_POSITIVEX 0x00000600 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_POSITIVEX
#define DDS_CUBEMAP_NEGATIVEX 0x00000a00 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_NEGATIVEX
#define DDS_CUBEMAP_POSITIVEY 0x00001200 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_POSITIVEY
#define DDS_CUBEMAP_NEGATIVEY 0x00002200 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_NEGATIVEY
#define DDS_CUBEMAP_POSITIVEZ 0x00004200 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_POSITIVEZ
#define DDS_CUBEMAP_NEGATIVEZ 0x00008200 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_NEGATIVEZ

#define DDS_CUBEMAP 0x00000200 // DDSCAPS2_CUBEMAP
#define DDS_CUBEMAP_ALLFACES ( DDS_CUBEMAP_POSITIVEX | DDS_CUBEMAP_NEGATIVEX |\
                               DDS_CUBEMAP_POSITIVEY | DDS_CUBEMAP_NEGATIVEY |\
                               DDS_CUBEMAP_POSITIVEZ | DDS_CUBEMAP_NEGATIVEZ )

#define DDSCAPS2_VOLUME 0x00200000
#ifndef MAKEFOURCC
#define MAKEFOURCC(ch0, ch1, ch2, ch3)                              \
                ((uint32_t)(uint8_t)(ch0) | ((uint32_t)(uint8_t)(ch1) << 8) |       \
                ((uint32_t)(uint8_t)(ch2) << 16) | ((uint32_t)(uint8_t)(ch3) << 24 ))
#endif /* defined(MAKEFOURCC) */
    #pragma pack(push,1)
    // 32 bytes
    struct DDS_PIXELFORMAT
    {
        uint32_t dwSize; // 32 bytes ?
        // Values which indicate what type of data is in the surface. 
        uint32_t dwFlags;
        // Four-character codes for specifying compressed or custom formats. Possible values include: DXT1, DXT2, DXT3, DXT4, or DXT5.
        uint32_t dwFourCC;
        // Valid when dwFlags includes DDPF_RGB, DDPF_LUMINANCE, or DDPF_YUV.
        uint32_t dwRGBBitCount;
        // Red (or lumiannce or Y) mask for reading color data. For instance, given the A8R8G8B8 format, the red mask would be 0x00ff0000.
        uint32_t dwRBitMask;
        uint32_t dwGBitMask;
        uint32_t dwBBitMask;
        uint32_t dwABitMask;
    };
    // 124 bytes + 4 magic bytes
    struct DDS_HEADER {
        uint32_t           dwSize;
        uint32_t           dwFlags;
        uint32_t           dwHeight;
        uint32_t           dwWidth;
        uint32_t           dwPitchOrLinearSize;
        uint32_t           dwDepth;
        uint32_t           dwMipMapCount;
        uint32_t           dwReserved1[11];
        DDS_PIXELFORMAT    ddspf;
        uint32_t           dwCaps;
        uint32_t           dwCaps2;
        uint32_t           dwCaps3;
        uint32_t           dwCaps4;
        uint32_t           dwReserved2;
    };

    #pragma pack(pop)
    // Magic 4 bytes
    // DDS_HEADER 124 bytes
    // Main Surface Data
    // ...
#define MAKE_PIXEL(color) (uint32_t(color.A * 255) << 24) | (uint32_t(color.R * 255) << 16) | (uint32_t(color.G * 255) << 8) | uint32_t(color.B * 255)
    void CubeMap::SaveDDS(const char* path)
    {
        FILE* dds = fopen(path, "wb");
        uint32_t dwMagic = MAKEFOURCC('D','D','S',' ');
        fwrite(&dwMagic, sizeof(uint32_t), 1, dds);
        DDS_HEADER header = {
            124,
            DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PITCH | DDSD_PIXELFORMAT | DDSD_MIPMAPCOUNT,
            mHeight,
            mWidth,
            4 * mWidth, // 4 bytes (RGBA 24Bit)
            0,
            mMipLevels,
            {0},
            {
                32,
                DDPF_RGB | DDPF_ALPHAPIXELS,
                0,
                32, // dwRGBBitCount
                0x00ff0000,
                0x0000ff00,
                0x000000ff,
                0xff000000,
            },
            DDSCAPS_COMPLEX | DDSCAPS_MIPMAP | DDSCAPS_TEXTURE, //dwCaps
            DDS_CUBEMAP | DDS_CUBEMAP_ALLFACES, // dwCaps2
            0, 0, 0
        };
        fwrite(&header, sizeof(DDS_HEADER), 1, dds);
        for (int face = 0; face < 6; face++)
        {
            for (int mip = 0; mip < mMipLevels; mip++)
            {
                uint32_t* pixels = new uint32_t[mFaces[face][mip].size()];
                int i = 0;
                for (auto& color : mFaces[face][mip])
                {
                    pixels[i] = MAKE_PIXEL(color);
                    i++;
                }
                fwrite(pixels, sizeof(uint32_t) * mFaces[face][mip].size(), 1, dds);
                delete[] pixels;
            }
        }
        fclose(dds);
    }
}