#include "RuntimeIBL.h"
#include <string>

extern std::vector<uint8_t> load_bytes(std::string const& path);

namespace AlexQPBR
{
    GpuConvolveProgram::GpuConvolveProgram(bool useRGBM)
        : mProg(0)
		, mUseRGBMEncode(useRGBM)
    {
        glewInit();
        mProg = glCreateProgram();

        auto vs = load_bytes("src/cubemap_filter.vs");
        auto fs = load_bytes("src/cubemap_filter.fs");

        GLuint vert = CreateShader(vs, GL_VERTEX_SHADER);
        GLuint frag = CreateShader(fs, GL_FRAGMENT_SHADER);

        GLint linked;
        glAttachShader(mProg, vert);
        glAttachShader(mProg, frag);
        glLinkProgram(mProg);
        glGetProgramiv(mProg, GL_LINK_STATUS, &linked);
        if (linked == GL_FALSE) {

        }
        else {

        }
        if (glIsProgram(mProg))
        {
            Unbind();
        }
    }

    GpuConvolveProgram::~GpuConvolveProgram()
    {
        if (glIsProgram(mProg)) {
            glDeleteProgram(mProg);
            mProg = 0;
        }
    }
	bool GpuConvolveProgram::UseRGBM() const
	{
		return mUseRGBMEncode;
	}
    void GpuConvolveProgram::Bind()
    {
        glUseProgram(mProg);
    }

    void GpuConvolveProgram::Unbind()
    {
        glUseProgram(0);
    }

    void GpuConvolveProgram::SetTexture(const char * Name, GLint Tex)
    {
        CacheBinding(Name);
        glUniform1i(mBinding[Name], Tex);
    }
    void GpuConvolveProgram::SetFloat(const char * Name, float Value)
    {
        CacheBinding(Name);
        glUniform1f(mBinding[Name], Value);    
    }
    void GpuConvolveProgram::SetInt(const char * Name, int Value)
    {
        CacheBinding(Name);
        glUniform1ui(mBinding[Name], Value);    
    }
    void GpuConvolveProgram::SetMatrix(const char * Name, const glm::mat4 & Matrx)
    {
        CacheBinding(Name);
        glUniformMatrix4fv(mBinding[Name], 1, GL_FALSE, (const float *)&Matrx);    
    }
    GLuint GpuConvolveProgram::CreateShader(std::vector<uint8_t> const& data, GLuint Stage)
    {
        GLint compiled;
        GLuint shader = glCreateShader(Stage);
        const char* src = (const char*)data.data();
        GLint length = (GLint)data.size();
        glShaderSource(shader, 1, &src, &length);
        glCompileShader(shader);
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (compiled == GL_FALSE) {
            GLint len;
            GLchar szError[4 * 1024];
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
            glGetShaderInfoLog(shader, sizeof(szError), &len, szError);
            glDeleteShader(shader);
            printf("Vertex Error: %s\n", szError);
            shader = -1;
        }
        return shader;
    }
    bool GpuConvolveProgram::CacheBinding(const char* Name)
    {
        if (mBinding.find(Name) == mBinding.end())
        {
            mBinding.insert({ Name, glGetUniformLocation(mProg, Name) });
            return true;
        }
        return false;
    }
}