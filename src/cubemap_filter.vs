#version 450                      														 
uniform mat4 _modelViewProjectionMatrix;
in vec3 _position;
in vec4 _texcoord;
out vec4 texcoord;
void main()
{                                                 
    gl_Position = _modelViewProjectionMatrix*vec4(_position, 1.0);
    gl_Position.y *= -1.0f;
    texcoord = _texcoord;
}