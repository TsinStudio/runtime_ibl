#ifndef __runtime_ibl_h__
#define __runtime_ibl_h__

#ifdef BUILD_LIB
#define RTIBL_API __declspec(dllexport)
#else
#define RTIBL_API __declspec(dllimport)
#endif

#if __cplusplus
extern "C" {
#endif
    typedef enum ibl_cubemap_face {
        ibl_cubemap_px,
        ibl_cubemap_nx,
        ibl_cubemap_py,
        ibl_cubemap_ny,
        ibl_cubemap_pz,
        ibl_cubemap_nz,
    } ibl_cubemap_face;

    struct ibl_color_rgba {
        float r, g, b, a;
    };

    typedef struct ibl_cubemap_impl* ibl_cubemap_t;
    RTIBL_API ibl_cubemap_t     ibl_hdr_load(const char* hdr_path);
    RTIBL_API ibl_cubemap_t     ibl_create_cubemap(int w, int h, int mips);
    RTIBL_API void              ibl_destroy_cubemap(ibl_cubemap_t c);
    RTIBL_API bool              ibl_cubemap_is_hdr(ibl_cubemap_t c);
    RTIBL_API int               ibl_cubemap_get_width(ibl_cubemap_t c);
    RTIBL_API int               ibl_cubemap_get_height(ibl_cubemap_t c);
    RTIBL_API int               ibl_cubemap_get_mips(ibl_cubemap_t c);
    RTIBL_API float             ibl_cubemap_get_max_illum(ibl_cubemap_t c);
	RTIBL_API bool              ibl_cubemap_is_rgbm_encoded(ibl_cubemap_t c);
    RTIBL_API ibl_color_rgba*   ibl_cubemap_get_pixels(ibl_cubemap_t c, enum ibl_cubemap_face face, int mip);
    RTIBL_API void              ibl_cubemap_set_pixels(ibl_cubemap_t c, enum ibl_cubemap_face face, int mip, const ibl_color_rgba* pixels);
	RTIBL_API void              ibl_cubemap_filter_and_generate_mips(ibl_cubemap_t src, ibl_cubemap_t dest, int samples, bool useRGBM, float maxRGBM);
    RTIBL_API void              ibl_cubemap_save_dds(ibl_cubemap_t c, const char* path);

    // opengl api 
    RTIBL_API int               ibl_cubemap_create_gl_handle(ibl_cubemap_t c);
    RTIBL_API int               ibl_cubemap_get_gl_handle(ibl_cubemap_t c);

#if __cplusplus
}
#endif
#endif