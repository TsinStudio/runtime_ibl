#pragma once

#include "GL/glew.h"
#include <vector>
#include <array>
#include <memory>
#include <string>
#include <unordered_map>
#include <glm/glm.hpp>

namespace AlexQPBR {

    enum EPixelFormat {
        EPF_ARGB32,
        EPF_RGBF32,
    };
    struct ColorARGB {
        float R, G, B, A;
    };

    class CubeMap {
    public:
        enum EFace {
            PositiveX = 0,
            NegativeX = 1,
            PositiveY = 2,
            NegativeY = 3,
            PositiveZ = 4,
            NegativeZ = 5
        };

        typedef std::vector<ColorARGB> ColorTex;
        typedef std::vector<ColorTex> ColorTexMipChain;
        
        CubeMap(int width, int height, int mips = 1);
        CubeMap(const char* path, bool inCylinder);
        ~CubeMap();

		bool UseRGBM() const;
		void SetEncodeRGBM(bool bRGBM, float maxRgbm);

        EPixelFormat GetPixelFormat() const;
        int GetWidth() const;
        int GetHeight() const;
        int GetLevels() const;

        const ColorARGB* GetPixels(EFace face, int mip) const;
        ColorARGB* GetPixels(EFace face, int mip);
        void SetPixels(EFace face, int mip, ColorARGB* pixels);

        GLuint CreateGLCubemap();
        void Bind();

        void SaveDDS(const char* path);

        GLuint GetGLHandle() const { return mGLHandle; }

        static CubeMap* LoadHDR(const char* hdr_path);
        float MaxIllumi() const;
        bool IsHDR() const;
    private:
        void GeneratePyramids();
		float mMaxRGBM;
		bool mUseRGBM;
        int mWidth;
        int mHeight;
        int mMipLevels;
        bool mHDR;
        bool mCylinder;
        float* mFloatData;
        float mMaxIllum;
        std::array<ColorTexMipChain,6> mFaces;

        GLuint mGLHandle;
    };

    class GpuConvolveProgram
    {
    public:
        GpuConvolveProgram(bool useRGBM);
        ~GpuConvolveProgram();

		bool UseRGBM() const;

        void Bind();
        void Unbind();
        void SetTexture(const char* Name, GLint Tex);
        void SetFloat(const char* Name, float Value);
        void SetInt(const char* Name, int Value);
        void SetMatrix(const char* Name, const glm::mat4& Matrx);
    private:
        GLuint CreateShader(std::vector<uint8_t> const& data, GLuint Stage);
        bool CacheBinding(const char* Name);
        std::unordered_map<std::string, GLuint> mBinding;
        GLuint mProg;

		bool mUseRGBMEncode;
    };

    class RenderTexture
    {
    public:
        RenderTexture(int width, int height);
        ~RenderTexture();

        void Bind();
        void Unbind();

    private:
        int mWidth;
        int mHeight;
        GLuint mFBO;
        GLuint mRBO;
        GLuint mTex;
    };

    class QuadRender {
    public:
        QuadRender();
        ~QuadRender();
        void Bind();
        void Unbind();
        void Draw();
    private:
        void InitBuffer();
        GLuint mIBO;
        GLuint mVBO;
    };

    class IBLRenderer {
    public:
        IBLRenderer(bool useRGBM = false, float maxRGBM = 1.0);
        ~IBLRenderer();

        void GenerateFilteredMipmapChain(CubeMap* src, CubeMap* dest, int samples);

	private:
		bool mUseRGBMEncode;
		float mMaxRGBM;
    };
}