#version 450																	 
#define PI 3.1415926535897932384626433832795f
#define UNITY_SPECCUBE_LOD_STEPS 6								 
uniform uint _samples;
uniform uint _level;
uniform float _maxRGBM;
//uniform samplerCube _envmap;
uniform sampler2D _sphericalMap;
uniform mat4 _texcoordMatrix;
in vec4 texcoord;

vec4 EncodeRGBM(vec3 color, float maxRGBM)
{
    float kOneOverRGBMMaxRange = 1.0 / maxRGBM;
    const float kMinMultiplier = 2.0 * 1e-2;
    vec3 rgb = color * kOneOverRGBMMaxRange;
    float alpha = max(max(rgb.r, rgb.g), max(rgb.b, kMinMultiplier));
    alpha = ceil(alpha * 255.0) / 255.0;
    // Division-by-zero warning from d3d9, so make compiler happy.
    alpha = max(alpha, kMinMultiplier);
    return vec4(rgb / alpha, alpha);
}

float RadicalInverse(uint bits)                                                          
{                                                                                        
    bits = (bits << 16u) | (bits >> 16u);                                                
    bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);                  
    bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);                  
    bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);                  
    bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);                  
    return float(bits) * 2.3283064365386963e-10;                                         
}

vec2 Hammersley(uint i, uint n)                                                          
{                                                                                        
    return vec2(1.0f * i / n, RadicalInverse(i));                                        
}

float MipmapLevelToPerceptualRoughness(uint level)
{
    float perceptualRoughness = clamp(float(level) / UNITY_SPECCUBE_LOD_STEPS, 0, 1);
    float root = (2.89f / 1.96f) - perceptualRoughness * (2.8f / 1.96f);
    float ret = 1.7f / 1.4f - sqrt(root);
    return clamp(ret, 0.0, 1.0);
}

vec3 ImportanceSamplingGGX(vec2 xi, vec3 normal, float roughness)                        
{                                                                                        
    float a = roughness * roughness;                                                     
                                                                                            
    float phi = 2.0f * PI * xi.x;                                                        
    float costheta = sqrt((1.0f - xi.y) / (1.0f + (a * a - 1.0f) * xi.y));               
    float sintheta = sqrt(1.0f - costheta * costheta);                                   
                                                                                            
    vec3 h = vec3(sintheta * cos(phi), sintheta * sin(phi), costheta);                   
    vec3 up = abs(normal.z) < 0.999f ? vec3(0.0f, 0.0f, 1.0f) : vec3(1.0f, 0.0f, 0.0f);  
    vec3 tx = normalize(cross(up, normal));                                              
    vec3 ty = cross(normal, tx);                                                         
                                                                                            
    return normalize(tx * h.x + ty * h.y + normal * h.z);                                
}

vec2 SphericalSample(vec3 R)
{
    float m = 2.0 * sqrt(pow(R.x, 2.0) + pow(R.y, 2.0) + pow(R.z + 1.0, 2.0));
    return R.xy / vec2(m) + vec2(0.5);
}

vec3 GammaToLinear(vec3 color)
{
    return pow(color, vec3(1.0f / 2.2f));
}

vec3 Sampling(sampler2D envmap, vec3 normal, float roughness, uint samples)            
{                                                                                        
    vec3 N = normal;                                                                     
    vec3 V = normal;                                                                     
                                                                                            
    float weight = 0.0f;                                                                 
    vec3 color = vec3(0.0f, 0.0f, 0.0f);                                                 
                                                                                            
    for (uint index = 0u; index < samples; index++)                                      
    {                                                                                    
        vec2 Xi = Hammersley(index, samples);                                            
        vec3 H = ImportanceSamplingGGX(Xi, N, roughness);                                
        vec3 L = normalize(dot(V, H) * H * 2.0f - V);								 
        float ndotl = max(dot(N, L), 0.0f);
        if (ndotl > 0.0f) {                                                              
            //color += pow(texture(envmap, L).rgb, vec3(1.0f / 2.2f)) * ndotl;
            vec3 o_color = texture(envmap, SphericalSample(L)).rgb;
            color += ndotl * GammaToLinear(o_color);
            weight += ndotl;                                                             
        }                                                                                
    }																		 
    color /= weight;                                                                     
    return color;                                                                        
}

void main()                                                                              
{                                                                                        
    vec4 direction = _texcoordMatrix * vec4(texcoord.x, texcoord.y, 1.0f, 0.0f);         
    direction.xyz = normalize(direction.xyz);
    float _roughness = MipmapLevelToPerceptualRoughness(_level);
    vec3 color = Sampling(_sphericalMap, direction.xyz, _roughness, _samples);           
    gl_FragColor = EncodeRGBM(color, _maxRGBM);
}